import serial
import time

class Arduino():

    conectado = 0

    def __init__(self):
        """
        Inicializa a classe arduino
        """
        self.conectado = 0

    def conectar(self, serial_port='COM3', baud_rate=9600, read_timeout=1):
        """tenta conectar usando os parametros dados"""
        if not self.conectado:
            self.conn = serial.Serial(serial_port, baud_rate, timeout=1)
            self.conn.timeout = read_timeout  # Timeout for readline()
        time.sleep(2)

    def verifica_status(self):
        """
        Envia 'f0000' e espera a resposta com o estado pro arduino
        """
        command = 'f0000\n'.encode()
        self.conn.write(command)
        try:
            recebido = int(self.conn.read())
            if (recebido == 7):
                self.conectado = 1
            self.limpar()
        except ValueError:
            self.conectado = 0

    def desconectar(self):
        """
        Encerra a comunicação
        """
        self.conn.close()
        self.conectado = 0

    def enviar(self,comando, valor):
        """
        Envia '1' para ligar o bombeamento
        """
        command = (comando+valor+'\n').encode()
        #print(command)
        self.conn.write(command)
        time.sleep(0.05)

    def enviar_especial(self,texto):
        """
        Envia '1' para ligar o bombeamento
        """
        command = (texto+'\n').encode()
        #print(command)
        self.conn.write(command)
        time.sleep(0.05)

    def receber(self):
        """
        Envia '0' para desligar o bombeamento
        """
        recebido = int(self.conn.readline())
        print(recebido)


    def limpar(self):
        """limpa o buffer do serial"""
        recebido = self.conn.readline()
        recebido = ""

if __name__ == '__main__':
    hardware = Arduino()
    hardware.conectar('COM3', 9600)
    hardware.verifica_status()
    hardware.desconectar()
