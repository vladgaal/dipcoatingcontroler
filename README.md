# DipCoatingControler
> Python based program to run the Dip Coating, usually applied in a Layer by Layer deposition. This repository includes the file of the pieces to be printed and assembly the hardware. The electronics is Arduino based and the circuit of the shield is also provided.

---

## Clone the repository

> If you don't yet have, [download and install git](https://git-scm.com/downloads). Use the git clone command to clone from: https://bitbucket.org/vladgaal/dipcoatingcontroler.git

---

## Install dependencies
**Windows users**

Download [here](https://www.python.org/downloads/windows/) and install the latest version of Python 3.7.
Go to the cloned folder ".\Dependencies" and execute the "install_all.bat" (just for x64 computers) to install everything python related that you need to run.
Go to the Arduino [page](https://www.arduino.cc/en/software), download and install the Arduino IDE and drivers.
 
**Linux users**

Install/update the following libraries:

> ```
> sudo -H pip3 install pyserial
> ```

To install PySide on Linux please read the [build instructions](https://pyside.readthedocs.io/en/latest/building/linux.html)
Go to the Arduino [page](https://www.arduino.cc/en/software), download and install the Arduino IDE and drivers.

---
## Run

Just execute "DipCoatingControler V0.1.exe" or from the command line (on the cloned folder):

> ```
> python main.py
> ```

---

## Important tips

Based on user experiences, we outlined a set of tips on how to get better results with PyLab:

1 - The 12Vdc power supply must be connected to work properly.

2 - There is no end stop sensor in this hardware therefore be careful whit the movements. 

3 - After coating fired by software is possible to close the program and disconnect the USB without injury.

4 - If the coating is running do not connect via usb, this procedure restarts the hardware.

---

## Release control

0.1 -> Initial development

0.3 -> New immersion time range from 1 to 999
