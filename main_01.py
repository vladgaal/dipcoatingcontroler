__appname__ = "Lbl Dip coating - Control "
__version__ = "V0.1"

from PySide.QtCore import *
from PySide.QtGui import *
import sys
from ui_files import mainDialog
import utilitario
import arduinoClass


class MainDialog(QDialog, mainDialog.Ui_Dialog):
    porta = ''
    baud = ''
    def __init__(self, parent=None):
        super(MainDialog, self).__init__(parent, Qt.WindowMinimizeButtonHint)
        self.setupUi(self)

        self.converte_box_dial_lavagem()
        self.converte_box_dial_secagem()

        self.setWindowTitle(__appname__+__version__)

        self.portaBox.clear()
        self.portaBox.addItems(utilitario.serial_ports())

        self.baudBox.clear()
        self.baudBox.addItems(["", "9600"])

        self.conectarBt.clicked.connect(self.conectar_arduino)
        self.desconectarBt.clicked.connect(self.desconectar_arduino)
        self.refreshBt.clicked.connect(self.atualizar_portas)

        self.conexaoTexto.setText("Hardware disconnected")
        self.conexaoTexto.setStyleSheet("QLabel#conexaoTexto {color: red;}")

        self.imersaoNegDial.sliderMoved.connect(self.imersaoNegBox.setValue)
        self.imersaoNegBox.valueChanged.connect(self.imersaoNegDial.setValue)

        self.imersaoPosDial.sliderMoved.connect(self.imersaoPosBox.setValue)
        self.imersaoPosBox.valueChanged.connect(self.imersaoPosDial.setValue)

        self.lavagemDial.sliderMoved.connect(self.converte_dial_box_lavagem)
        self.lavagemBox.valueChanged.connect(self.converte_box_dial_lavagem)

        self.secagemDial.sliderMoved.connect(self.converte_dial_box_secagem)
        self.secagemBox.valueChanged.connect(self.converte_box_dial_secagem)

        self.setBt.clicked.connect(self.rotina)
        self.enviarBt.clicked.connect(self.especial)

        self.upBt.clicked.connect(self.mover_up)
        self.downBt.clicked.connect(self.mover_down)
        self.leftBt.clicked.connect(self.mover_left)
        self.rightBt.clicked.connect(self.mover_right)

        self.comandoEspecial.returnPressed.connect(self.enviarBt.setFocus)

    def nao_conectado(self):
        QMessageBox.warning(self, "Communication problem", "The hardware is not connected!", QMessageBox.Ok)

    def especial(self):
        texto = self.comandoEspecial.text()
        if len(texto) == 5:
            if texto[0] in ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q']:
                try:
                    valor = int(texto[1:])
                    comando = texto[0]
                    arduino.enviar(comando, texto[1:])
                    self.comandoEspecial.setFocus()
                except ValueError:
                    QMessageBox.warning(self, "Unable to send",
                                        "The last 4 digits must be numbers.\nPlease read the communication protocol.",
                                        QMessageBox.Ok)
                except AttributeError:
                    self.nao_conectado()

                except:
                    QMessageBox.warning(self, "Unable to send",
                                        "Unable to send the command.\nPlease read the communication protocol.",
                                        QMessageBox.Ok)

            else:
                QMessageBox.warning(self, "Unable to send",
                                    "This letter does not match a command.\nPlease read the communication protocol.",
                                    QMessageBox.Ok)
        else:
            QMessageBox.warning(self, "Unable to send",
                                "The entered command is out of standard.\nPlease read the communication protocol.",
                                QMessageBox.Ok)
        self.comandoEspecial.clear()

    def mover_up(self):
        try:
            valor = utilitario.box_to_str(self.verticalBox.value())
            arduino.enviar('n', valor)
            arduino.enviar('a', '0000')
        except AttributeError:
            self.nao_conectado()

    def mover_down(self):
        try:
            valor = utilitario.box_to_str(self.verticalBox.value())
            arduino.enviar('n', valor)
            arduino.enviar('b', '0000')
        except AttributeError:
            self.nao_conectado()

    def mover_left(self):
        try:
            valor = utilitario.box_to_str(self.anguloBox.value())
            arduino.enviar('l', valor)
            arduino.enviar('c', '0000')
        except AttributeError:
            self.nao_conectado()

    def mover_right(self):
        try:
            valor = utilitario.box_to_str(self.anguloBox.value())
            arduino.enviar('l', valor)
            arduino.enviar('d', '0000')

        except AttributeError:
            self.nao_conectado()

    def rotina(self):
        try:
            comando = ['g', 'h', 'i', 'j', 'k', 'l', 'n', 'e']

            valor = [utilitario.box_to_str(self.imersaoPosBox.value()),
                     utilitario.box_to_str(self.imersaoNegBox.value()),
                     utilitario.box_to_str(int(10*(self.lavagemBox.value()))),
                     utilitario.box_to_str(int(10*(self.secagemBox.value()))),
                     utilitario.box_to_str(self.bicamadasBox.value()),
                     utilitario.box_to_str(self.anguloBox.value()),
                     utilitario.box_to_str(self.verticalBox.value()),
                     '0000']

            for a in range(len(comando)):
                arduino.enviar(comando[a], valor[a])

        except AttributeError:
            self.nao_conectado()

    def recebe(self):
        arduino.receber()

    def converte_dial_box_lavagem(self):
        """converte o valor inteiro do dial para o box cm decimal"""
        valor = self.lavagemDial.value()
        self.lavagemBox.setValue(valor/10)

    def converte_box_dial_lavagem(self):
        """converte o valor com decimal do bo para inteiro do dial"""
        valor = self.lavagemBox.value()
        self.lavagemDial.setValue(int(valor*10))

    def converte_dial_box_secagem(self):
        """converte o valor inteiro do dial para o box cm decimal"""
        valor = self.secagemDial.value()
        self.secagemBox.setValue(valor/10)

    def converte_box_dial_secagem(self):
        """converte o valor com decimal do bo para inteiro do dial"""
        valor = self.secagemBox.value()
        self.secagemDial.setValue(int(valor*10))

    def atualizar_portas(self):
        self.portaBox.clear()
        self.portaBox.addItems(utilitario.serial_ports())

    def conectar_arduino(self):
        """pega os setups e tenta conectar o arduino"""
        porta = self.portaBox.currentText()
        baud = self.baudBox.currentText()

        if porta == "":
            QMessageBox.warning(self, "Failed to connect", "Select a valid port.", QMessageBox.Ok)
            return
        if baud == "":
            QMessageBox.warning(self, "Failed to connect", "Select a valid Baud Rate.", QMessageBox.Ok)
            return
        arduino.conectar(porta, baud)
        arduino.verifica_status()
        self.verifica_arduino_conectado()
        self.setBt.setFocus()
        if not arduino.conectado:
            QMessageBox.warning(self, "Failed to connect", "Could not connect using the specified settings.",
                                QMessageBox.Ok)
            arduino.desconectar()

    def desconectar_arduino(self):
        """desconecta o arduino"""
        arduino.desconectar()
        self.verifica_arduino_conectado()

    def verifica_arduino_conectado(self):
        """verifica se o arduino esta conectado"""
        if arduino.conectado:
            self.conexaoTexto.setText("Hardware connected")
            self.conexaoTexto.setStyleSheet("QLabel#conexaoTexto {color: rgb(88, 255, 116);}")
        else:
            self.conexaoTexto.setText("Hardware disconnected")
            self.conexaoTexto.setStyleSheet("QLabel#conexaoTexto {color: red;}")

    def closeEvent(self, event, *args, **kwargs):
        """Ritual de encerramento"""
        result = QMessageBox.question(self, __appname__, "Are you sure you want to exit?", QMessageBox.Yes | QMessageBox.No,
                                      QMessageBox.Yes)
        if result == QMessageBox.Yes:
            """Colocar aqui tudo que tiver que ser feito antes de sair"""
            if arduino.conectado:
                arduino.desconectar()
            event.accept()
        else:
            event.ignore()


if __name__ == '__main__':
    QCoreApplication.setApplicationName(__appname__)
    QCoreApplication.setApplicationVersion(__version__)
    QCoreApplication.setOrganizationName("IFGW Unicamp")
    QCoreApplication.setOrganizationDomain("portal.ifi.unicamp.br")

    arduino = arduinoClass.Arduino()
    app = QApplication(sys.argv)
    form = MainDialog()
    form.show()
    app.exec_()