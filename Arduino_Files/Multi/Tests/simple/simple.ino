#include <String.h>
#include "TimerOne.h"

#define N_BICAMADAS 30  // número de bicamadas
#define TEMPO_IMERSO_P 0.1 //em minutos
#define TEMPO_IMERSO_N 0.1 //em minutos
#define TEMPO_LAVAGEM 5 //em segundos
#define TEMPO_SECAGEM 5 //em segundos
#define TEMPO_ESPERA 1 //em segundos
#define DESL_VERTICAL 20 // deslocamento vertical em mm
#define DESL_LATERAL 30 //ângulo de deslocamento lateral em graus


#define SEGUNDO  1000   //conversão segundo para msegundo
#define MINUTO 60000    //conversão minuto para msegundo
#define PITCH 0.8
#define CARRO_ENABLE 11 //enable motor de cima
#define CARRO_DIR 13    //dir motor de cima
#define CARRO_STEP 12   //step motor de cima
#define BRACO_STEP 9    //step motor de baixo
#define BRACO_ENABLE 10 //enable motor de baixo
#define BRACO_DIR 8     //dir motor de baixo
#define LIGADO LOW
#define DESLIGADO HIGH
#define SUBIR HIGH
#define DESCER LOW
#define ESQUERDA LOW
#define DIREITA HIGH
#define ERRO 10
#define LENTO 0.3         //1 para lento de vdd, 100% do tempo de pulso
#define RAPIDO 0.3      //30% do tempo de pulso
#define DEBOUNCE 50

int n_bicamadas = N_BICAMADAS; // número de bicamadas

int angulo = DESL_LATERAL;               //ângulo de deslocamento lateral

long tempo_imerso_p = TEMPO_IMERSO_P * MINUTO;   //tempo de imersao do postivo
long tempo_imerso_n = TEMPO_IMERSO_N * MINUTO;   //tempo de imersao do negativo
long tempo_secagem = TEMPO_SECAGEM * MINUTO;     //tempo de secagem
long tempo_lavagem = TEMPO_LAVAGEM * SEGUNDO;    //tempo de lavagem da rotina
long tempo_espera = TEMPO_ESPERA * SEGUNDO;     //tempo de espera

//CUIDADO! O # de voltas deve ser sempre inteiro.
float d1 = VERTICAL_DESL;                           // deslocamento vertical em mm
int voltas_vertical = (int)(d1 / PITCH); // # voltas para deslocamento d1
float grau_por_passo_motor = 1.8;
float grau_por_passo = grau_por_passo_motor / 8;
int passos_por_volta = (int)360 / grau_por_passo;

int v_up = 10;        //velocidade de subida em mm/min
int v_down = 60;      //velocidade de descida em mm/min
int v_angular = 5;    //velocidade de angular
double t_up = (24E6) / (v_up*passos_por_volta);         // tempo de subida
double t_down = (24E6) / (v_down*passos_por_volta);     // tempo de descida
double t_angular = (24E6) / (v_angular*passos_por_volta); // velocidade para girar

int numero = 0;
int contagem = 0;

void setup() {
  pinMode(CARRO_ENABLE, OUTPUT);
  pinMode(CARRO_STEP, OUTPUT);
  pinMode(CARRO_DIR, OUTPUT);
  pinMode(BRACO_ENABLE, OUTPUT);
  pinMode(BRACO_STEP, OUTPUT);
  pinMode(BRACO_DIR, OUTPUT);
  digitalWrite(CARRO_ENABLE, DESLIGADO); //Desliga o motor do carro
  digitalWrite(BRACO_ENABLE, DESLIGADO); //Desliga o motor do bracinho
  converte();
}

void loop() {
  if (analogRead(A0) > 256){
      rotina_principal();
    }
}

void up_motion(float multiplicador) {
  digitalWrite(BRACO_ENABLE, LIGADO);  //Liga o motor do braço
  digitalWrite(BRACO_DIR, SUBIR);
  for (int k = 0; k < voltas_vertical; k++) {
    for (int x = 0; x < passos_por_volta; x++) {
      passo(BRACO_STEP, t_up * multiplicador);
    }
  }
  digitalWrite(BRACO_ENABLE, DESLIGADO);  //Desliga o motor do braço
}

void down_motion(float multiplicador) {
  digitalWrite(BRACO_ENABLE, LIGADO); //Liga o motor do braço
  digitalWrite(BRACO_DIR, DESCER);//SOBE COM LOW
  for (int k = 0; k < voltas_vertical; k++) {
    for (int x = 0; x < passos_por_volta ; x++) {
      passo(BRACO_STEP, t_down * multiplicador);
    }
  }
  digitalWrite(BRACO_ENABLE, DESLIGADO);  //Desliga o motor do braço
}

void right_motion(float multiplicador) {
  digitalWrite(CARRO_ENABLE, LIGADO);        //Liga o motor do carro
  digitalWrite(CARRO_DIR, DIREITA);

  for (float x = 0; x <= angulo; x = x + grau_por_passo) {
    passo(CARRO_STEP, t_angular * multiplicador);
  }

  digitalWrite(CARRO_ENABLE, DESLIGADO);    //Desliga o motor do carro
}

void left_motion(float multiplicador) {
  digitalWrite(CARRO_ENABLE, LIGADO);        //Liga o motor do carro
  digitalWrite(CARRO_DIR, ESQUERDA);

  for (float x = 0; x <= angulo; x = x + grau_por_passo) {
    passo(CARRO_STEP, t_angular * multiplicador);
  }

  digitalWrite(CARRO_ENABLE, DESLIGADO);      // DEsliga o motor do carro
}

void rotina_principal() {
  numero = 0;
  mostrar = true;
  for (int y = 0; y < n_bicamadas; y++) {
    numero = y;
    down_motion(LENTO);
    delay(tempo_imerso_p);

    up_motion(LENTO);
    delay(tempo_espera);

    right_motion(LENTO);
    delay(tempo_espera);

    down_motion(LENTO);
    delay(tempo_lavagem);

    up_motion(LENTO);
    delay(tempo_secagem);

    right_motion(LENTO);
    delay(tempo_espera);

    down_motion(LENTO);
    delay(tempo_imerso_n);

    up_motion(LENTO);
    delay(tempo_espera);

    left_motion(LENTO);
    delay(tempo_espera);

    down_motion(LENTO);
    delay(tempo_lavagem);

    up_motion(LENTO);
    delay(tempo_secagem);

    left_motion(LENTO);
  }
  mostrar = false;
}

void converte() {
  passos_por_volta = (int)360 / grau_por_passo;
  voltas_vertical = (int)(d1 / PITCH);
  grau_por_passo = grau_por_passo_motor / 8;
}

void passo(int pino, float tempo) {
  digitalWrite(pino, HIGH);
  delayMicroseconds(tempo);
  digitalWrite(pino, LOW);
  delayMicroseconds(tempo);
}
