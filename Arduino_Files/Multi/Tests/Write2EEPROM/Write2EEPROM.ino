#include <EEPROM.h>

#define N_BICAMADAS 30
#define TEMPO_IMERSO_P 120
#define TEMPO_IMERSO_N 6
#define TEMPO_LAVAGEM 5
#define TEMPO_SECAGEM 5
#define TEMPO_ESPERA 1
#define DESL_VERTICAL 20
#define DESL_LATERAL 30
#define VEL_DESCIDA 60
#define VEL_SUBIDA 10
#define VEL_ANGULAR 5
#define TEMPO_IMERSO_MONO 5
#define TEMPO_SECAGEM_MONO 5

#define SECOND  1000L   //conversão segundo para msegundo

long immersed_time_p = 120 * SECOND;

void setup() {
  Serial.begin(9600);
  delay(500);
  Serial.println(TEMPO_IMERSO_P);
  Serial.println(immersed_time_p);
  Serial.println(immersed_time_p / SECOND);
  EEPROM.write(0, N_BICAMADAS); // N_BICAMADAS
  EEPROM.write(1, int(immersed_time_p / SECOND)); // TEMPO_IMERSO_P
  EEPROM.write(2, TEMPO_IMERSO_N); // TEMPO_IMERSO_N
  EEPROM.write(3, TEMPO_LAVAGEM); // TEMPO_LAVAGEM
  EEPROM.write(4, TEMPO_SECAGEM); // TEMPO_SECAGEM
  EEPROM.write(5, TEMPO_ESPERA); // TEMPO_ESPERA
  EEPROM.write(6, DESL_VERTICAL); // DESL_VERTICAL
  EEPROM.write(7, DESL_LATERAL); // DESL_LATERAL
  EEPROM.write(8, VEL_DESCIDA); // VEL_DESCIDA
  EEPROM.write(9, VEL_SUBIDA); // VEL_SUBIDA
  EEPROM.write(10, VEL_ANGULAR); // VEL_ANGULAR
  EEPROM.write(11, TEMPO_IMERSO_MONO); // TEMPO_IMERSO_MONO
  EEPROM.write(12, TEMPO_SECAGEM_MONO); // TEMPO_SECAGEM_MONO
}

void loop() {
  Serial.println("inicio");
  Serial.println(EEPROM.read(0));
  Serial.println(EEPROM.read(1));
  Serial.println(EEPROM.read(2));
  Serial.println(EEPROM.read(3));
  Serial.println(EEPROM.read(4));
  Serial.println(EEPROM.read(5));
  Serial.println(EEPROM.read(6));
  Serial.println(EEPROM.read(7));
  Serial.println(EEPROM.read(8));
  Serial.println(EEPROM.read(9));
  Serial.println(EEPROM.read(10));
  Serial.println(EEPROM.read(11));
  Serial.println(EEPROM.read(12));
  Serial.println("fim");
  delay(5000);
}
