#include <String.h>
#include "TimerOne.h"
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <EEPROM.h>

#define N_BICAMADAS 30   // número de bicamadas
#define TEMPO_IMERSO_P 6 // em segundos
#define TEMPO_IMERSO_N 6 // em segundos
#define TEMPO_LAVAGEM 5  // em segundos
#define TEMPO_SECAGEM 5  // em segundos
#define TEMPO_ESPERA 1   // em segundos
#define DESL_VERTICAL 20 // deslocamento vertical em mm
#define DESL_LATERAL 30  // ângulo de deslocamento lateral em graus
#define VEL_DESCIDA 60   // velocidade de descida mm/min
#define VEL_SUBIDA 10    // velocidade de subida mm/min
#define VEL_ANGULAR 5    // velocidade angular grad/min
#define TEMPO_IMERSO_MONO 5 // em segundos
#define TEMPO_SECAGEM_MONO 5  // em segundos

#define SECOND  1000L   //conversão segundo para msegundo
#define MINUTE 60000    //conversão minuto para msegundo
#define PITCH 0.8
#define CAR_ENABLE 11 //enable motor de cima
#define CAR_DIR 13    //dir motor de cima
#define CAR_STEP 12   //step motor de cima
#define ARM_STEP 9    //step motor de baixo
#define ARM_ENABLE 10 //enable motor de baixo
#define ARM_DIR 8     //dir motor de baixo
#define ON LOW
#define OFF HIGH
#define UP HIGH
#define DOWN LOW
#define SETLEFT LOW
#define SETRIGHT HIGH
#define SLOW 1         //1 para lento de vdd, 100% do tempo de pulso
#define FAST 0.3      //30% do tempo de pulso
#define DEBOUNCE 200

#define BT_UP 566         //botao subida
#define BT_DOWN 931       //botao descida
#define BT_EXEC 841       //botao executa rotina
#define BT_LEFT 611       //botao esquerda
#define BT_RIGHT 699      //botao direita
#define BT_ERROR 10       //botao faixa de erro

int n_layer = N_BICAMADAS;          // número de bicamadas
long immersed_time_p = TEMPO_IMERSO_P * SECOND;   //tempo de imersao do postivo
long immersed_time_n = TEMPO_IMERSO_N * SECOND;   //tempo de imersao do negativo
long wash_time = TEMPO_LAVAGEM * SECOND;     //tempo de lavagem da rotina
long drying_time = TEMPO_SECAGEM * SECOND;    //tempo de secagem
long waiting_time = TEMPO_ESPERA * SECOND;       //tempo de espera
int displacement = DESL_VERTICAL;
int angle = DESL_LATERAL;       //ângulo de deslocamento lateral
int down_speed = VEL_DESCIDA;       //velocidade de descida em mm/min
int up_speed = VEL_SUBIDA;          //velocidade de subida em mm/min
int angular_speed = VEL_ANGULAR;    //velocidade de angular grad/min
long mono_immersed_time = TEMPO_IMERSO_MONO * SECOND;   //tempo de imersao no monolayer
long mono_drying_time = TEMPO_SECAGEM_MONO * SECOND; //tempo de secagem em monolayer

//CUIDADO! O # de voltas deve ser sempre inteiro.
int vertical_turns = (int)(displacement / PITCH); // # voltas para deslocamento
float degree_per_step_motor = 1.8;
float degree_per_step = degree_per_step_motor / 8;
int steps_per_revolution = (int)360 / degree_per_step;

double t_up = (24E6) / (up_speed*steps_per_revolution);         // tempo de subida
double t_down = (24E6) / (down_speed*steps_per_revolution);     // tempo de descida
double t_angular = (24E6) / (angular_speed*steps_per_revolution); // velocidade para girar
int layer = 0;
int menu = 0;
int line = 0;

String inputString = "";         // a string to hold incoming data
boolean stringComplete = false;
int valor_recebido = 0;

//LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);
LiquidCrystal_I2C lcd(0x27, 16, 2);

void setup() {
  Serial.begin(9600);
  //lcd.begin (16, 2);
  lcd.init();
  pinMode(CAR_ENABLE, OUTPUT);
  pinMode(CAR_STEP, OUTPUT);
  pinMode(CAR_DIR, OUTPUT);
  pinMode(ARM_ENABLE, OUTPUT);
  pinMode(ARM_STEP, OUTPUT);
  pinMode(ARM_DIR, OUTPUT);
  digitalWrite(CAR_ENABLE, OFF); //Desliga o motor do carro
  digitalWrite(ARM_ENABLE, OFF); //Desliga o motor do bracinho
  // writeEEPROM(); // Just on first burn
  readEEPROM();
  convert();
  inputString.reserve(200);
  lcd.setBacklight(HIGH);
  lcd.noBlink();
  animation();
  menu = 0;
  line = 0;
  set_lcd();
  //printSerial(); //tirar
}

void loop() {
  int bt = get_bt();
  if (bt != 0) {
    if (menu == 0) {
      if (line == 0) {
        if (bt == 2) line++;
        else if (bt == 5)  {
          delay(500);
          autoCoatingBi();
        }
      }
      else if (line == 1) {
        if (bt == 1) line--;
        else if (bt == 2) line++;
        else if (bt == 5)  {
          delay(500);
          autoCoatingMono();
        }
      }
      else if (line == 2) {
        if (bt == 1) line--;
        else if (bt == 2) line++;
        else if (bt == 5) {
          delay(500);
          manualCoating();
        }
      }
      else if (line == 3) {
        if (bt == 1) line--;
        else if (bt == 2) line++;
        else if (bt == 5) {
          delay(500);
          manualPosition();
        }
      }
      else if (line == 4) {
        if (bt == 1) line--;
        else if (bt == 2) line++;
        else if (bt == 5) {
          delay(500);
          bySoftware();
        }
      }
      else if (line == 5) {
        if (bt == 1) line--;
        else if (bt == 5) {
          delay(500);
          configuration();
        }
      }
    }
    set_lcd();
    delay(DEBOUNCE);
  }
}


void set_lcd() {
  lcd.clear();
  if ((menu == 0) && (line == 0)) {
    lcd.setCursor(0, 0);
    lcd.print("Auto Bilayer");
    lcd.setCursor(0, 1);
    lcd.print("Auto Monolayer");
    lcd.blink();
    lcd.setCursor(0, 0);
  }
  if ((menu == 0) && (line == 1)) {
    lcd.setCursor(0, 0);
    lcd.print("Auto Monolayer");
    lcd.setCursor(0, 1);
    lcd.print("Manual Coating");
    lcd.blink();
    lcd.setCursor(0, 0);
  }
  if ((menu == 0) && (line == 2)) {
    lcd.setCursor(0, 0);
    lcd.print("Manual Coating");
    lcd.setCursor(0, 1);
    lcd.print("Manual Position");
    lcd.blink();
    lcd.setCursor(0, 0);
  }
  if ((menu == 0) && (line == 3)) {
    lcd.setCursor(0, 0);
    lcd.print("Manual position");
    lcd.setCursor(0, 1);
    lcd.print("Software Control");
    lcd.blink();
    lcd.setCursor(0, 0);
  }
  if ((menu == 0) && (line == 4)) {
    lcd.setCursor(0, 0);
    lcd.print("Software Control");
    lcd.setCursor(0, 1);
    lcd.print("Configuration");
    lcd.blink();
    lcd.setCursor(0, 0);
  }
  if ((menu == 0) && (line == 5)) {
    lcd.setCursor(0, 0);
    lcd.print("Configuration");
    lcd.blink();
    lcd.setCursor(0, 0);
  }
  if ((menu == 1) && (line == 0)) {
    lcd.setCursor(0, 0);
    lcd.print("Manual position");
    lcd.setCursor(0, 1);
    lcd.print("Return");
    lcd.blink();
    lcd.setCursor(0, 1);
  }
  if ((menu == 2) && (line == 0)) {
    lcd.setCursor(0, 0);
    lcd.print("Set parameters");
    lcd.setCursor(0, 1);
    lcd.print("Read EEPROM");
    lcd.blink();
    lcd.setCursor(0, 0);
  }
  if ((menu == 2) && (line == 1)) {
    lcd.setCursor(0, 0);
    lcd.print("Read EEPROM");
    lcd.setCursor(0, 1);
    lcd.print("Write EEPROM");
    lcd.blink();
    lcd.setCursor(0, 0);
  }
  if ((menu == 2) && (line == 2)) {
    lcd.setCursor(0, 0);
    lcd.print("Write EEPROM");
    lcd.setCursor(0, 1);
    lcd.print("Print");
    lcd.blink();
    lcd.setCursor(0, 0);
  }
  if ((menu == 2) && (line == 3)) {
    lcd.setCursor(0, 0);
    lcd.print("Print");
    lcd.setCursor(0, 1);
    lcd.print("Return");
    lcd.blink();
    lcd.setCursor(0, 0);
  }
  if ((menu == 2) && (line == 4)) {
    lcd.setCursor(0, 0);
    lcd.print("Return");
    lcd.blink();
    lcd.setCursor(0, 0);
  }
  if ((menu == 3) && (line == 0)) {
    lcd.setCursor(0, 0);
    lcd.print(" Values loaded");
    lcd.setCursor(0, 1);
    lcd.print("  into EEPROM");
    lcd.setCursor(0, 0);
    lcd.blink();
  }
  if ((menu == 3) && (line == 1)) {
    lcd.setCursor(0, 0);
    lcd.print(" Values saved");
    lcd.setCursor(0, 1);
    lcd.print("   in EEPROM");
    lcd.setCursor(0, 0);
    lcd.blink();
  }
  if ((menu == 4) && (line == 0)) {
    lcd.setCursor(0, 0);
    lcd.print("Number of layers");
    lcd.setCursor(0, 1);
    lcd.print("Positive time");
    lcd.setCursor(0, 0);
    lcd.blink();
  }
  if ((menu == 4) && (line == 1)) {
    lcd.setCursor(0, 0);
    lcd.print("Positive time");
    lcd.setCursor(0, 1);
    lcd.print("Negaitive time");
    lcd.setCursor(0, 0);
    lcd.blink();
  }
  if ((menu == 4) && (line == 2)) {
    lcd.setCursor(0, 0);
    lcd.print("Negaitive time");
    lcd.setCursor(0, 1);
    lcd.print("Wash time");
    lcd.setCursor(0, 0);
    lcd.blink();
  }
  if ((menu == 4) && (line == 3)) {
    lcd.setCursor(0, 0);
    lcd.print("Wash time");
    lcd.setCursor(0, 1);
    lcd.print("Drying time");
    lcd.setCursor(0, 0);
    lcd.blink();
  }
  if ((menu == 4) && (line == 4)) {
    lcd.setCursor(0, 0);
    lcd.print("Drying time");
    lcd.setCursor(0, 1);
    lcd.print("Waiting time");
    lcd.setCursor(0, 0);
    lcd.blink();
  }
  if ((menu == 4) && (line == 5)) {
    lcd.setCursor(0, 0);
    lcd.print("Waiting time");
    lcd.setCursor(0, 1);
    lcd.print("Vertical displ.");
    lcd.setCursor(0, 0);
    lcd.blink();
  }
  if ((menu == 4) && (line == 6)) {
    lcd.setCursor(0, 0);
    lcd.print("Vertical displ.");
    lcd.setCursor(0, 1);
    lcd.print("Angular  displ.");
    lcd.setCursor(0, 0);
    lcd.blink();
  }
  if ((menu == 4) && (line == 7)) {
    lcd.setCursor(0, 0);
    lcd.print("Angular  displ.");
    lcd.setCursor(0, 1);
    lcd.print("Descent speed");
    lcd.setCursor(0, 0);
    lcd.blink();
  }
  if ((menu == 4) && (line == 8)) {
    lcd.setCursor(0, 0);
    lcd.print("Descent speed");
    lcd.setCursor(0, 1);
    lcd.print("Ascent speed");
    lcd.setCursor(0, 0);
    lcd.blink();
  }
  if ((menu == 4) && (line == 9)) {
    lcd.setCursor(0, 0);
    lcd.print("Ascent speed");
    lcd.setCursor(0, 1);
    lcd.print("Angular speed");
    lcd.setCursor(0, 0);
    lcd.blink();
  }
  if ((menu == 4) && (line == 10)) {
    lcd.setCursor(0, 0);
    lcd.print("Angular speed");
    lcd.setCursor(0, 1);
    lcd.print("Mono time");
    lcd.setCursor(0, 0);
    lcd.blink();
  }
  if ((menu == 4) && (line == 11)) {
    lcd.setCursor(0, 0);
    lcd.print("Mono time");
    lcd.setCursor(0, 1);
    lcd.print("Mono drying time");
    lcd.setCursor(0, 0);
    lcd.blink();
  }
  if ((menu == 4) && (line == 12)) {
    lcd.setCursor(0, 0);
    lcd.print("Mono drying time");
    lcd.setCursor(0, 1);
    lcd.print("Return");
    lcd.setCursor(0, 0);
    lcd.blink();
  }
  if ((menu == 4) && (line == 13)) {
    lcd.setCursor(0, 0);
    lcd.print("Return");
    lcd.setCursor(0, 0);
    lcd.blink();
  }
  if ((menu == 5) && (line == 0)) {
    lcd.setCursor(0, 0);
    lcd.print("Manual coating");
    lcd.setCursor(0, 1);
    lcd.print("Return");
    lcd.blink();
    lcd.setCursor(0, 1);
  }
  if ((menu == 6) && (line == 0)) {
    lcd.setCursor(0, 0);
    lcd.print("Waiting commands ");
    lcd.setCursor(0, 1);
    lcd.print("Return");
    lcd.blink();
    lcd.setCursor(0, 1);
  }
}

int get_bt() {
  int  leitura = analogRead(A0);
  if ((leitura > (BT_UP - BT_ERROR)) && (leitura < (BT_UP + BT_ERROR))) return 1;
  else if ((leitura > (BT_DOWN - BT_ERROR)) && (leitura < (BT_DOWN + BT_ERROR))) return 2;
  else if ((leitura > (BT_LEFT - BT_ERROR)) && (leitura < (BT_LEFT + BT_ERROR))) return 3;
  else if ((leitura > (BT_RIGHT - BT_ERROR)) && (leitura < (BT_RIGHT + BT_ERROR))) return 4;
  else if ((leitura > (BT_EXEC - BT_ERROR)) && (leitura < (BT_EXEC + BT_ERROR))) return 5;
  else return 0;
}

void up_motion(float multiplicador) {
  convert();
  digitalWrite(ARM_ENABLE, ON);  //Liga o motor do braço
  digitalWrite(ARM_DIR, UP);
  for (int k = 0; k < vertical_turns; k++) {
    for (int x = 0; x < steps_per_revolution; x++) {
      steps(ARM_STEP, t_up * multiplicador);
    }
  }
  //digitalWrite(ARM_ENABLE, OFF);  //Desliga o motor do braço
}

void down_motion(float multiplicador) {
  convert();
  digitalWrite(ARM_ENABLE, ON); //Liga o motor do braço
  digitalWrite(ARM_DIR, DOWN);//SOBE COM LOW
  for (int k = 0; k < vertical_turns; k++) {
    for (int x = 0; x < steps_per_revolution ; x++) {
      steps(ARM_STEP, t_down * multiplicador);
    }
  }
  //digitalWrite(ARM_ENABLE, OFF);  //Desliga o motor do braço
}

void right_motion(float multiplicador) {
  convert();
  digitalWrite(CAR_ENABLE, ON);        //Liga o motor do carro
  digitalWrite(CAR_DIR, SETRIGHT);

  for (float x = 0; x <= angle; x = x + degree_per_step) {
    steps(CAR_STEP, t_angular * multiplicador);
  }

  //digitalWrite(CAR_ENABLE, OFF);    //Desliga o motor do carro
}

void left_motion(float multiplicador) {
  convert();
  digitalWrite(CAR_ENABLE, ON);        //Liga o motor do carro
  digitalWrite(CAR_DIR, SETLEFT);

  for (float x = 0; x <= angle; x = x + degree_per_step) {
    steps(CAR_STEP, t_angular * multiplicador);
  }

  //digitalWrite(CAR_ENABLE, OFF);      // DEsliga o motor do carro
}

void autoCoatingBi() {
  lcd.clear();
  lcd.noBlink();

  for (int y = 0; y < n_layer; y++) {
    layer = y;
    int remTime = (int) remainingTimeCalc(2) / 60;
    remTime = remTime * (n_layer - layer);
    int hour = (int) remTime / 60;
    int minute = remTime - ((int) remTime / 60) * 60;
    char timeBuffer [6];
    sprintf(timeBuffer, "%02d:%02d", hour, minute);

    lcd.setCursor(0, 0);
    lcd.print("Layer ");
    lcd.print(layer);
    lcd.print(" of ");
    lcd.print(n_layer);
    lcd.setCursor(0, 1);
    lcd.print("Time left ");
    lcd.print(timeBuffer);

    down_motion(SLOW);
    delay(immersed_time_p);

    up_motion(SLOW);
    delay(waiting_time);

    right_motion(SLOW);
    delay(waiting_time);

    down_motion(SLOW);
    delay(wash_time);

    up_motion(SLOW);
    delay(drying_time);

    right_motion(SLOW);
    delay(waiting_time);

    down_motion(SLOW);
    delay(immersed_time_n);

    up_motion(SLOW);
    delay(waiting_time);

    left_motion(SLOW);
    delay(waiting_time);

    down_motion(SLOW);
    delay(wash_time);

    up_motion(SLOW);
    delay(drying_time);

    left_motion(SLOW);
    delay(waiting_time);
  }
}


void autoCoatingMono() {
  lcd.clear();
  lcd.noBlink();

  for (int y = 0; y < n_layer; y++) {
    layer = y;

    int remTime = (int) remainingTimeCalc(1) / 60;
    remTime = remTime * (n_layer - layer);
    int hour = (int) remTime / 60;
    int minute = remTime - ((int) remTime / 60) * 60;
    char timeBuffer [6];
    sprintf(timeBuffer, "%02d:%02d", hour, minute);

    lcd.setCursor(0, 0);
    lcd.print("Layer ");
    lcd.print(layer);
    lcd.print(" of ");
    lcd.print(n_layer);
    lcd.setCursor(0, 1);
    lcd.print("Time left ");
    lcd.print(timeBuffer);

    down_motion(SLOW);
    delay(mono_immersed_time);

    up_motion(SLOW);
    delay(mono_drying_time);
  }
}

void manualCoating() {
  menu = 5;
  line = 0;
  set_lcd();
  convert();
  boolean keep = true;
  while (keep == true) {
    int bt = get_bt();
    if (bt != 0) {
      if (bt == 1) up_motion(SLOW);
      else if (bt == 2) down_motion(SLOW);
      else if (bt == 3) left_motion(SLOW);
      else if (bt == 4) right_motion(SLOW);
      else if (bt == 5) keep = false;
    }
    delay(DEBOUNCE);
  }
  menu = 0;
  line = 0;
}


void manualPosition() {
  int displacementTemp = displacement;
  int angleTemp = angle;
  displacement = 1;
  angle = 10;
  convert();
  menu = 1;
  line = 0;
  set_lcd();
  boolean keep = true;
  while (keep == true) {
    int bt = get_bt();
    if (bt != 0) {
      if (bt == 1) up_motion(FAST);
      else if (bt == 2) down_motion(FAST);
      else if (bt == 3) left_motion(FAST);
      else if (bt == 4) right_motion(FAST);
      else if (bt == 5) keep = false;
    }
    delay(DEBOUNCE);
  }
  menu = 0;
  line = 0;
  displacement = displacementTemp;
  angle = angleTemp;
  convert();
}


void configuration() {
  menu = 2;
  line = 0;
  set_lcd();
  boolean keep = true;
  while (keep == true) {
    int bt = get_bt();
    if (bt != 0) {
      if (line == 0) {
        if (bt == 2) line = 1;
        else if (bt == 5) {
          delay(500);
          setParameters();
        }
      }
      else if (line == 1) {
        if (bt == 1) line = 0;
        else if (bt == 2) line = 2;
        else if (bt == 5) {
          readEEPROM();
          menu = 3;
          line = 0;
          set_lcd();
          delay(2000);
          menu = 2;
          line = 1;
          set_lcd();
        }
      }
      else if (line == 2) {
        if (bt == 1) line = 1;
        else if (bt == 2) line = 3;
        else if (bt == 5) {
          writeEEPROM();
          menu = 3;
          line = 1;
          set_lcd();
          delay(2000);
          menu = 2;
          line = 2;
          set_lcd();
        }
      }
      else if (line == 3) {
        if (bt == 1) line = 2;
        else if (bt == 2) line = 4;
        else if (bt == 5) printSerial();
      }
      else if (line == 4) {
        if (bt == 1) line = 3;
        else if (bt == 5) keep = false;
      }
      set_lcd();
    }
    delay(100);
  }
  menu = 0;
  line = 0;
}


void writeEEPROM() {
  writeIntIntoEEPROM(0 * 5, n_layer);
  writeIntIntoEEPROM(1 * 5, int(immersed_time_p / SECOND));
  writeIntIntoEEPROM(2 * 5, int(immersed_time_n / SECOND));
  writeIntIntoEEPROM(3 * 5, int(wash_time / SECOND));
  writeIntIntoEEPROM(4 * 5, int(drying_time / SECOND));
  writeIntIntoEEPROM(5 * 5, int(waiting_time / SECOND));
  writeIntIntoEEPROM(6 * 5, displacement);
  writeIntIntoEEPROM(7 * 5, angle);
  writeIntIntoEEPROM(8 * 5, down_speed);
  writeIntIntoEEPROM(9 * 5, up_speed);
  writeIntIntoEEPROM(10 * 5, angular_speed);
  writeIntIntoEEPROM(11 * 5, int(mono_immersed_time / SECOND));
  writeIntIntoEEPROM(12 * 5, int(mono_drying_time / SECOND));
}


void readEEPROM() {
  n_layer = readIntFromEEPROM(0 * 5);    // número de bicamadas
  immersed_time_p = readIntFromEEPROM(1 * 5) * SECOND; //tempo de imersao do postivo
  immersed_time_n = readIntFromEEPROM(2 * 5) * SECOND; //tempo de imersao do negativo
  wash_time = readIntFromEEPROM(3 * 5) * SECOND;  //tempo de lavagem da rotina
  drying_time = readIntFromEEPROM(4 * 5) * SECOND; //tempo de secagem
  waiting_time = readIntFromEEPROM(5 * 5) * SECOND;   //tempo de espera
  displacement = readIntFromEEPROM(6 * 5);
  angle = readIntFromEEPROM(7 * 5);     //ângulo de deslocamento lateral
  down_speed = readIntFromEEPROM(8 * 5);    //velocidade de descida em mm/min
  up_speed = readIntFromEEPROM(9 * 5);      //velocidade de subida em mm/min
  angular_speed = readIntFromEEPROM(10 * 5); //velocidade de angular
  mono_immersed_time = readIntFromEEPROM(11 * 5) * SECOND;
  mono_drying_time = readIntFromEEPROM(12 * 5) * SECOND;
}


void writeIntIntoEEPROM(int address, int number)
{
  EEPROM.write(address, number >> 8);
  EEPROM.write(address + 1, number & 0xFF);
}

int readIntFromEEPROM(int address)
{
  byte byte1 = EEPROM.read(address);
  byte byte2 = EEPROM.read(address + 1);
  return (byte1 << 8) + byte2;
}

void printSerial() {
  Serial.println(n_layer);
  Serial.println(immersed_time_p);
  Serial.println(immersed_time_n);
  Serial.println(wash_time);
  Serial.println(drying_time);
  Serial.println(waiting_time);
  Serial.println(displacement);
  Serial.println(angle);
  Serial.println(down_speed);
  Serial.println(up_speed);
  Serial.println(angular_speed);
  Serial.println(mono_immersed_time);
  Serial.println(mono_drying_time);
}


void setParameters() {
  menu = 4;
  line = 0;
  set_lcd();
  boolean keep = true;
  long temp = 0;
  while (keep == true) {
    int bt = get_bt();
    if (bt != 0) {
      if (line == 0) {
        if (bt == 2) line++;
        else if (bt == 5) {
          delay(500);
          lcd.clear();
          lcd.noBlink();
          lcd.setCursor(0, 0);
          lcd.print("Number of layers");
          lcd.setCursor(2, 1);
          lcd.print("#");
          char buff [6];
          sprintf(buff, "%02d", n_layer);
          lcd.setCursor(0, 1);
          lcd.print(buff);
          boolean keepKeeping = true;
          while (keepKeeping == true) {
            int bt = get_bt();
            if (bt != 0) {
              if (bt == 1) n_layer++;
              else if (bt == 2) n_layer--;
              else if (bt == 5) keepKeeping = false;
              char buff [6];
              sprintf(buff, "%02d", n_layer);
              lcd.setCursor(0, 1);
              lcd.print(buff);
            }
            delay(DEBOUNCE);
          }
        }
      }

      else if (line == 1) {
        if (bt == 1) line--;
        else if (bt == 2)  line++;
        else if (bt == 5) {
          delay(500);
          lcd.clear();
          lcd.noBlink();
          lcd.setCursor(0, 0);
          lcd.print("Positive time");
          lcd.setCursor(15, 1);
          lcd.print("s");
          //char buff [6];
          //sprintf(buff, "%04d", (int) immersed_time_p / SECOND);
          lcd.setCursor(0, 1);
          lcd.print("      ");
          lcd.setCursor(0, 1);
          temp = immersed_time_p / SECOND;
          lcd.print(temp);
          boolean keepKeeping = true;
          while (keepKeeping == true) {
            int bt = get_bt();
            if (bt != 0) {
              if (bt == 1) immersed_time_p = immersed_time_p + 1000;
              else if (bt == 2) immersed_time_p = immersed_time_p - 1000;
              else if (bt == 5) keepKeeping = false;
              //char buff [6];
              //sprintf(buff, "%04d", (int) immersed_time_p / SECOND);
              lcd.setCursor(0, 1);
              lcd.print("      ");
              lcd.setCursor(0, 1);
              temp = immersed_time_p / SECOND;
              lcd.print(temp);
            }
            delay(DEBOUNCE);
          }
        }
      }
      else if (line == 2) {
        if (bt == 1) line--;
        else if (bt == 2)  line++;
        else if (bt == 5) {
          delay(500);
          lcd.clear();
          lcd.noBlink();
          lcd.setCursor(0, 0);
          lcd.print("Negative time");
          lcd.setCursor(2, 1);
          lcd.print("s");
          //char buff [6];
          //sprintf(buff, "%04d", (int) immersed_time_n / SECOND);
          lcd.setCursor(0, 1);
          lcd.print("      ");
          lcd.setCursor(0, 1);
          temp = immersed_time_n / SECOND;
          lcd.print(temp);
          boolean keepKeeping = true;
          while (keepKeeping == true) {
            int bt = get_bt();
            if (bt != 0) {
              if (bt == 1) immersed_time_n = immersed_time_n + 1000;
              else if (bt == 2) immersed_time_n = immersed_time_n - 1000;
              else if (bt == 5) keepKeeping = false;
              //char buff [6];
              //sprintf(buff, "%04d", (int) immersed_time_n / SECOND);
              lcd.setCursor(0, 1);
              lcd.print("      ");
              lcd.setCursor(0, 1);
              temp = immersed_time_n / SECOND;
              lcd.print(temp);
            }
            delay(DEBOUNCE);
          }
        }
      }
      else if (line == 3) {
        if (bt == 1) line--;
        else if (bt == 2)  line++;
        else if (bt == 5) {
          delay(500);
          lcd.clear();
          lcd.noBlink();
          lcd.setCursor(0, 0);
          lcd.print("Wash time");
          lcd.setCursor(2, 1);
          lcd.print("s");
          //char buff [6];
          //sprintf(buff, "%04d", (int) wash_time / SECOND);
          lcd.setCursor(0, 1);
          lcd.print("      ");
          lcd.setCursor(0, 1);
          temp = wash_time / SECOND;
          lcd.print(temp);
          boolean keepKeeping = true;
          while (keepKeeping == true) {
            int bt = get_bt();
            if (bt != 0) {
              if (bt == 1) wash_time = wash_time + 1000;
              else if (bt == 2) wash_time = wash_time - 1000;
              else if (bt == 5) keepKeeping = false;
              //char buff [6];
              //sprintf(buff, "%04d", (int) wash_time / SECOND);
              lcd.setCursor(0, 1);
              lcd.print("      ");
              lcd.setCursor(0, 1);
              temp = wash_time / SECOND;
              lcd.print(temp);
            }
            delay(DEBOUNCE);
          }
        }
      }
      else if (line == 4) {
        if (bt == 1) line--;
        else if (bt == 2)  line++;
        else if (bt == 5) {
          delay(500);
          lcd.clear();
          lcd.noBlink();
          lcd.setCursor(0, 0);
          lcd.print("Drying time");
          lcd.setCursor(2, 1);
          lcd.print("s");
          //char buff [6];
          //sprintf(buff, "%04d", (int) drying_time / SECOND);
          lcd.setCursor(0, 1);
          lcd.print("      ");
          lcd.setCursor(0, 1);
          temp = drying_time / SECOND;
          lcd.print(temp);
          boolean keepKeeping = true;
          while (keepKeeping == true) {
            int bt = get_bt();
            if (bt != 0) {
              if (bt == 1) drying_time = drying_time + 1000;
              else if (bt == 2) drying_time = drying_time - 1000;
              else if (bt == 5) keepKeeping = false;
              //char buff [6];
              //sprintf(buff, "%04d", (int) drying_time / SECOND);
              lcd.setCursor(0, 1);
              lcd.print("      ");
              lcd.setCursor(0, 1);
              temp = drying_time / SECOND;
              lcd.print(temp);
            }
            delay(DEBOUNCE);
          }
        }
      }
      else if (line == 5) {
        if (bt == 1) line--;
        else if (bt == 2)  line++;
        else if (bt == 5) {
          delay(500);
          lcd.clear();
          lcd.noBlink();
          lcd.setCursor(0, 0);
          lcd.print("Waiting time");
          lcd.setCursor(2, 1);
          lcd.print("s");
          //char buff [6];
          //sprintf(buff, "%04d", (int) waiting_time / SECOND);
          lcd.setCursor(0, 1);
          lcd.print("      ");
          lcd.setCursor(0, 1);
          temp = waiting_time / SECOND;
          lcd.print(temp);
          boolean keepKeeping = true;
          while (keepKeeping == true) {
            int bt = get_bt();
            if (bt != 0) {
              if (bt == 1) waiting_time = waiting_time + 1000;
              else if (bt == 2) waiting_time = waiting_time - 1000;
              else if (bt == 5) keepKeeping = false;
              //char buff [6];
              //sprintf(buff, "%04d", (int) waiting_time / SECOND);
              lcd.setCursor(0, 1);
              lcd.print("      ");
              lcd.setCursor(0, 1);
              temp = waiting_time / SECOND;
              lcd.print(temp);
            }
            delay(DEBOUNCE);
          }
        }
      }
      else if (line == 6) {
        if (bt == 1) line--;
        else if (bt == 2)  line++;
        else if (bt == 5) {
          delay(500);
          lcd.clear();
          lcd.noBlink();
          lcd.setCursor(0, 0);
          lcd.print("Vertical displ.");
          lcd.setCursor(2, 1);
          lcd.print("mm");
          //char buff [6];
          //sprintf(buff, "%02d", displacement);
          lcd.setCursor(0, 1);
          lcd.print("      ");
          lcd.setCursor(0, 1);
          lcd.print(displacement);
          boolean keepKeeping = true;
          while (keepKeeping == true) {
            int bt = get_bt();
            if (bt != 0) {
              if (bt == 1) displacement++;
              else if (bt == 2) displacement--;
              else if (bt == 5) keepKeeping = false;
              //char buff [6];
              //sprintf(buff, "%02d", displacement);
              lcd.setCursor(0, 1);
              lcd.print("      ");
              lcd.setCursor(0, 1);
              lcd.print(displacement);
            }
            delay(DEBOUNCE);
          }
        }
      }
      else if (line == 7) {
        if (bt == 1) line--;
        else if (bt == 2)  line++;
        else if (bt == 5) {
          delay(500);
          lcd.clear();
          lcd.noBlink();
          lcd.setCursor(0, 0);
          lcd.print("Angular displ.");
          lcd.setCursor(2, 1);
          lcd.print("grad");
          char buff [6];
          sprintf(buff, "%02d", angle);
          lcd.setCursor(0, 1);
          lcd.print(buff);
          boolean keepKeeping = true;
          while (keepKeeping == true) {
            int bt = get_bt();
            if (bt != 0) {
              if (bt == 1) angle++;
              else if (bt == 2) angle--;
              else if (bt == 5) keepKeeping = false;
              char buff [6];
              sprintf(buff, "%02d", angle);
              lcd.setCursor(0, 1);
              lcd.print(buff);
            }
            delay(DEBOUNCE);
          }
        }
      }
      else if (line == 8) {
        if (bt == 1) line--;
        else if (bt == 2)  line++;
        else if (bt == 5) {
          delay(500);
          lcd.clear();
          lcd.noBlink();
          lcd.setCursor(0, 0);
          lcd.print("Descent speed");
          lcd.setCursor(2, 1);
          lcd.print("mm/min");
          char buff [6];
          sprintf(buff, "%02d", down_speed);
          lcd.setCursor(0, 1);
          lcd.print(buff);
          boolean keepKeeping = true;
          while (keepKeeping == true) {
            int bt = get_bt();
            if (bt != 0) {
              if (bt == 1) down_speed++;
              else if (bt == 2) down_speed--;
              else if (bt == 5) keepKeeping = false;
              char buff [6];
              sprintf(buff, "%02d", down_speed);
              lcd.setCursor(0, 1);
              lcd.print(buff);
            }
            delay(DEBOUNCE);
          }
        }
      }
      else if (line == 9) {
        if (bt == 1) line--;
        else if (bt == 2)  line++;
        else if (bt == 5) {
          delay(500);
          lcd.clear();
          lcd.noBlink();
          lcd.setCursor(0, 0);
          lcd.print("Ascent speed");
          lcd.setCursor(2, 1);
          lcd.print("mm/min");
          char buff [6];
          sprintf(buff, "%02d", up_speed);
          lcd.setCursor(0, 1);
          lcd.print(buff);
          boolean keepKeeping = true;
          while (keepKeeping == true) {
            int bt = get_bt();
            if (bt != 0) {
              if (bt == 1) up_speed++;
              else if (bt == 2) up_speed--;
              else if (bt == 5) keepKeeping = false;
              char buff [6];
              sprintf(buff, "%02d", up_speed);
              lcd.setCursor(0, 1);
              lcd.print(buff);
            }
            delay(DEBOUNCE);
          }
        }
      }
      else if (line == 10) {
        if (bt == 1) line--;
        else if (bt == 2)  line++;
        else if (bt == 5) {
          delay(500);
          lcd.clear();
          lcd.noBlink();
          lcd.setCursor(0, 0);
          lcd.print("Angular speed");
          lcd.setCursor(2, 1);
          lcd.print("grad/min");
          char buff [6];
          sprintf(buff, "%02d", angular_speed);
          lcd.setCursor(0, 1);
          lcd.print(buff);
          boolean keepKeeping = true;
          while (keepKeeping == true) {
            int bt = get_bt();
            if (bt != 0) {
              if (bt == 1) angular_speed++;
              else if (bt == 2) angular_speed--;
              else if (bt == 5) keepKeeping = false;
              char buff [6];
              sprintf(buff, "%02d", angular_speed);
              lcd.setCursor(0, 1);
              lcd.print(buff);
            }
            delay(DEBOUNCE);
          }
        }
      }
      else if (line == 11) {
        if (bt == 1) line--;
        else if (bt == 2)  line++;
        else if (bt == 5) {
          delay(500);
          lcd.clear();
          lcd.noBlink();
          lcd.setCursor(0, 0);
          lcd.print("Mono time");
          lcd.setCursor(2, 1);
          lcd.print("s");
          //char buff [6];
          //sprintf(buff, "%02d", mono_immersed_time / SECOND);
          lcd.setCursor(0, 1);
          lcd.print("      ");
          lcd.setCursor(0, 1);
          temp = mono_immersed_time / SECOND;
          lcd.print(temp);
          boolean keepKeeping = true;
          while (keepKeeping == true) {
            int bt = get_bt();
            if (bt != 0) {
              if (bt == 1) mono_immersed_time = mono_immersed_time + 1000;
              else if (bt == 2) mono_immersed_time = mono_immersed_time - 1000;
              else if (bt == 5) keepKeeping = false;
              //char buff [6];
              //sprintf(buff, "%02d", mono_immersed_time / SECOND);
              lcd.setCursor(0, 1);
              lcd.print("      ");
              lcd.setCursor(0, 1);
              temp = mono_immersed_time / SECOND;
              lcd.print(temp);
            }
            delay(DEBOUNCE);
          }
        }
      }
      else if (line == 12) {
        if (bt == 1) line--;
        else if (bt == 2)  line++;
        else if (bt == 5) {
          delay(500);
          lcd.clear();
          lcd.noBlink();
          lcd.setCursor(0, 0);
          lcd.print("Mono drying time");
          lcd.setCursor(2, 1);
          lcd.print("s");
          //char buff [6];
          //sprintf(buff, "%02d", (int) mono_drying_time / SECOND);
          lcd.setCursor(0, 1);
          lcd.print("      ");
          lcd.setCursor(0, 1);
          temp = mono_drying_time / SECOND;
          lcd.print(temp);
          boolean keepKeeping = true;
          while (keepKeeping == true) {
            int bt = get_bt();
            if (bt != 0) {
              if (bt == 1) mono_drying_time = mono_drying_time + 1000;
              else if (bt == 2) mono_drying_time = mono_drying_time - 1000;
              else if (bt == 5) keepKeeping = false;
              //char buff [6];
              //sprintf(buff, "%02d", (int) mono_drying_time / SECOND);
              lcd.setCursor(0, 1);
              lcd.print("      ");
              lcd.setCursor(0, 1);
              temp = mono_drying_time / SECOND;
              lcd.print(temp);
            }
            delay(DEBOUNCE);
          }
        }
      }
      else if (line == 13) {
        if (bt == 1)  line--;
        else if (bt == 5) keep = false;
      }
      set_lcd();
    }
    delay(DEBOUNCE);
  }
  menu = 2;
  line = 0;
}


void convert() {
  steps_per_revolution = (int)360 / degree_per_step;
  vertical_turns = (int)(displacement / PITCH);
  degree_per_step = degree_per_step_motor / 8;
}


void steps(int pin, float timeMark) {
  digitalWrite(pin, HIGH);
  delayMicroseconds(timeMark);
  digitalWrite(pin, LOW);
  delayMicroseconds(timeMark);
}


int remainingTimeCalc(int type) {
  // 1 for monolayer
  // 2 for bilayer
  // return seconds
  int t = 0;
  if (type == 1) {
    t = t + (int) 60 * displacement / (down_speed * SLOW);
    t = t + (int) mono_immersed_time / SECOND;
    t = t + (int) 60 * displacement / (up_speed * SLOW);
    t = t + (int) mono_drying_time / SECOND;
    return t;
  }
  else if (type == 2) {
    t = t + (int) 60 * displacement / (down_speed * SLOW);
    t = t + (int) immersed_time_p / SECOND;
    t = t + (int) 60 * displacement / (up_speed * SLOW);
    t = t + (int) waiting_time / SECOND;
    t = t + (int) 60 * angle / (angular_speed * SLOW);
    t = t + (int) waiting_time / SECOND;
    t = t + (int) 60 * displacement / (down_speed * SLOW);
    t = t + (int) wash_time / SECOND;
    t = t + (int) 60 * displacement / (up_speed * SLOW);
    t = t + (int) drying_time / SECOND;
    t = t + (int) 60 * angle / (angular_speed * SLOW);
    t = t + (int) waiting_time / SECOND;
    t = t + (int) 60 * displacement / (down_speed * SLOW);
    t = t + (int) immersed_time_n / SECOND;
    t = t + (int) 60 * displacement / (up_speed * SLOW);
    t = t + (int) waiting_time / SECOND;
    t = t + (int) 60 * angle / (angular_speed * SLOW);
    t = t + (int) waiting_time / SECOND;
    t = t + (int) 60 * displacement / (down_speed * SLOW);
    t = t + (int) wash_time / SECOND;
    t = t + (int) 60 * displacement / (up_speed * SLOW);
    t = t + (int) drying_time / SECOND;
    t = t + (int) 60 * angle / (angular_speed * SLOW);
    t = t + (int) waiting_time / SECOND;
    return t;
  }
  else {
    return -1;
  }
}


void animation() {
  byte dip1a[8] = {
    B11100,
    B00100,
    B00100,
    B01110,
    B01010,
    B01010,
    B01110,
  };

  byte dip1b[8] = {
    B10001,
    B10001,
    B10001,
    B11111,
    B11111,
    B11111,
    B11111,
  };

  byte dip2a[8] = {
    B00000,
    B11100,
    B00100,
    B00100,
    B01110,
    B01010,
    B01010,
  };

  byte dip2b[8] = {
    B11111,
    B10001,
    B10001,
    B11111,
    B11111,
    B11111,
    B11111,
  };

  byte dip3a[8] = {
    B00000,
    B00000,
    B11100,
    B00100,
    B00100,
    B01110,
    B01010,
  };

  byte dip3b[8] = {
    B11011,
    B11111,
    B10001,
    B11111,
    B11111,
    B11111,
    B11111,
  };

  byte dip4a[8] = {
    B00000,
    B00000,
    B00000,
    B11100,
    B00100,
    B00100,
    B01110,
  };

  byte dip4b[8] = {
    B11011,
    B11011,
    B11111,
    B11111,
    B11111,
    B11111,
    B11111,
  };

  byte dip5a[8] = {
    B00000,
    B00000,
    B00000,
    B00000,
    B11100,
    B00100,
    B00100,
  };

  byte dip5b[8] = {
    B11111,
    B11011,
    B11011,
    B11111,
    B11111,
    B11111,
    B11111,
  };

  byte dip6a[8] = {
    B00000,
    B00000,
    B00000,
    B00000,
    B00000,
    B11100,
    B00100,
  };

  byte dip6b[8] = {
    B10101,
    B11111,
    B11011,
    B11111,
    B11111,
    B11111,
    B11111,
  };
  byte dip7a[8] = {
    B00000,
    B00000,
    B00000,
    B00000,
    B00000,
    B00000,
    B11100,
  };

  byte dip7b[8] = {
    B10101,
    B10101,
    B11111,
    B11111,
    B11111,
    B11111,
    B11111,
  };
  int timeFun = 400;
  lcd.createChar(0, dip1a);
  lcd.createChar(1, dip1b);
  lcd.createChar(2, dip2a);
  lcd.createChar(3, dip2b);
  lcd.createChar(4, dip3a);
  lcd.createChar(5, dip3b);
  lcd.createChar(6, dip4a);
  lcd.createChar(7, dip4b);
  lcd.setCursor(0, 0);
  lcd.print("Dip-coating");
  lcd.setCursor(0, 1);
  lcd.print("   Multi V4");
  lcd.setCursor(14, 0);
  lcd.write(byte(0));
  lcd.setCursor(14, 1);
  lcd.write(byte(1));
  delay(timeFun);
  lcd.setCursor(14, 0);
  lcd.write(byte(2));
  lcd.setCursor(14, 1);
  lcd.write(byte(3));
  delay(timeFun);
  lcd.setCursor(14, 0);
  lcd.write(byte(4));
  lcd.setCursor(14, 1);
  lcd.write(byte(5));
  delay(timeFun);
  lcd.setCursor(14, 0);
  lcd.write(byte(6));
  lcd.setCursor(14, 1);
  lcd.write(byte(7));
  delay(timeFun);
  lcd.createChar(0, dip5a);
  lcd.createChar(1, dip5b);
  lcd.createChar(2, dip6a);
  lcd.createChar(3, dip6b);
  lcd.createChar(4, dip7a);
  lcd.createChar(5, dip7b);
  lcd.setCursor(14, 0);
  lcd.write(byte(0));
  lcd.setCursor(14, 1);
  lcd.write(byte(1));
  delay(timeFun);
  lcd.setCursor(14, 0);
  lcd.write(byte(2));
  lcd.setCursor(14, 1);
  lcd.write(byte(3));
  delay(timeFun);
  lcd.setCursor(14, 0);
  lcd.write(byte(4));
  lcd.setCursor(14, 1);
  lcd.write(byte(5));
  delay(timeFun * 2);
  lcd.setCursor(14, 0);
  lcd.write(byte(4));
  lcd.setCursor(14, 1);
  lcd.write(byte(5));
  delay(timeFun);
  lcd.setCursor(14, 0);
  lcd.write(byte(2));
  lcd.setCursor(14, 1);
  lcd.write(byte(3));
  delay(timeFun);
  lcd.setCursor(14, 0);
  lcd.write(byte(0));
  lcd.setCursor(14, 1);
  lcd.write(byte(1));
  delay(timeFun);
  lcd.createChar(0, dip4a);
  lcd.createChar(1, dip4b);
  lcd.createChar(2, dip3a);
  lcd.createChar(3, dip3b);
  lcd.createChar(4, dip2a);
  lcd.createChar(5, dip2b);
  lcd.createChar(6, dip1a);
  lcd.createChar(7, dip1b);
  lcd.setCursor(14, 0);
  lcd.write(byte(0));
  lcd.setCursor(14, 1);
  lcd.write(byte(1));
  delay(timeFun);
  lcd.setCursor(14, 0);
  lcd.write(byte(2));
  lcd.setCursor(14, 1);
  lcd.write(byte(3));
  delay(timeFun);
  lcd.setCursor(14, 0);
  lcd.write(byte(4));
  lcd.setCursor(14, 1);
  lcd.write(byte(5));
  delay(timeFun);
  lcd.setCursor(14, 0);
  lcd.write(byte(6));
  lcd.setCursor(14, 1);
  lcd.write(byte(7));
  delay(timeFun);
}

int bySoftware() {
  menu = 6;
  line = 0;
  set_lcd();
  boolean keep = true;
  while (keep == true) {
    while (Serial.available()) {
      char inChar = (char)Serial.read();
      inputString += inChar;
      if (inChar == '\n') {
        stringComplete = true;
      }
    }
    if (stringComplete) {
      if (inputString.length() == 6) {
        int milhar = (int)inputString.charAt(1) - 48;
        int centena = (int)inputString.charAt(2) - 48;
        int dezena = (int)inputString.charAt(3) - 48;
        int unidade = (int)inputString.charAt(4) - 48;
        valor_recebido = milhar * 1000 + centena * 100 + dezena * 10 + unidade;

        switch (inputString.charAt(0)) {
          case 'a':
            up_motion(FAST);
            break;
          case 'b':
            down_motion(FAST);
            break;
          case 'c':
            left_motion(FAST);
            break;
          case 'd':
            right_motion(FAST);
            break;
          case 'e':
            autoCoatingBi();
            break;
          case 'f':
            Serial.println("7");
            break;
          case 'g':
            immersed_time_p = valor_recebido * MINUTE;
            break;
          case 'h':
            immersed_time_n = valor_recebido * MINUTE;
            break;
          case 'i':
            wash_time = SECOND * valor_recebido / 10;
            break;
          case 'j':
            drying_time = MINUTE * valor_recebido / 10;
            break;
          case 'k':
            n_layer = valor_recebido;
            break;
          case 'l':
            angle = valor_recebido;
            break;
          case 'm':
            degree_per_step_motor = valor_recebido / 10;
            break;
          case 'n':
            displacement = valor_recebido;
            break;
          case 'o':
            up_speed = valor_recebido;
            break;
          case 'p':
            down_speed = valor_recebido;
            break;
          case 'q':
            angular_speed = valor_recebido;
            break;
          default:
            break;
        }
        convert();
      }
      // clear the string:
      inputString = "";
      stringComplete = false;
    }
    int bt = get_bt();
    if (bt != 0) {
      if (bt == 5) keep = false;
      set_lcd();
    }
    delay(1000);
  }
  menu = 0;
  line = 0;
}

void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag
    // so the main loop can do something about it:
    if (inChar == '\n') {
      stringComplete = true;
    }
  }
}

void serialEventRun(void) {
  if (Serial.available()) serialEvent();
}
