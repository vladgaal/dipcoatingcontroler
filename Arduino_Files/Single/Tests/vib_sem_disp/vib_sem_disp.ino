#include <String.h>

#define N_BICAMADAS 30  // número de bicamadas
#define SEGUNDO  1000   //conversão segundo para msegundo
#define MINUTO 60000    //conversão minuto para msegundo
#define PITCH 0.8
#define CARRO_ENABLE 11 //enable motor de cima
#define CARRO_DIR 13    //dir motor de cima
#define CARRO_STEP 12   //step motor de cima
#define BRACO_STEP 9    //step motor de baixo
#define BRACO_ENABLE 10 //enable motor de baixo
#define BRACO_DIR 8     //dir motor de baixo
#define LIGADO LOW
#define DESLIGADO HIGH
#define SUBIR LOW
#define DESCER HIGH
#define ESQUERDA HIGH
#define DIREITA LOW
#define VIB_ENABLE_PIN 2 
#define ERRO 10
#define UP 167
#define DOWN 854
#define LEFT 335
#define RIGHT 673
#define SET 504
#define BT_UP 1
#define BT_DOWN 2
#define BT_LEFT 3
#define BT_RIGHT 4
#define BT_SET 5
#define LENTO 1         //100% do tempo de pulso
#define RAPIDO 0.3      //30% do tempo de pulso
#define DEBOUNCE 50

int n_bicamadas = N_BICAMADAS; // número de bicamadas

int angulo = 30;               //ângulo de deslocamento lateral

long tempo_imerso_p = 10 * MINUTO;   //tempo de imersao do postivo
long tempo_imerso_n = 10 * MINUTO;   //tempo de imersao do negativo
long tempo_secagem = 1 * MINUTO;     //tempo de secagem
long tempo_lavagem = 1 * SEGUNDO;    //tempo de lavagem da rotina
long tempo_espera = 1 * SEGUNDO;     //tempo de espera

//CUIDADO! O # de voltas deve ser sempre inteiro.
float d1 = 20;                           // deslocamento vertical em mm
int voltas_vertical = (int)(d1 / PITCH); // # voltas para deslocamento d1
float grau_por_passo_motor = 1.8;
float grau_por_passo = grau_por_passo_motor / 8;
int passos_por_volta = (int)360 / grau_por_passo;

int v_up = 10;        //velocidade de subida em mm/min
int v_down = 60;      //velocidade de descida em mm/min
int v_angular = 5;    //velocidade de angular
double t_up = (24E6) / (v_up*passos_por_volta);         // tempo de subida
double t_down = (24E6) / (v_down*passos_por_volta);     // tempo de descida
double t_angular = (24E6) / (v_angular*passos_por_volta); // velocidade para girar

String inputString = "";         // a string to hold incoming data
boolean stringComplete = false;  // whether the string is complete
String comando = "";
int valor_recebido = 0;

int numero = 0;
int disp = 1;
int contagem = 0;

void setup() {
  Serial.begin(9600);
  pinMode(CARRO_ENABLE, OUTPUT);
  pinMode(CARRO_STEP, OUTPUT);
  pinMode(CARRO_DIR, OUTPUT);
  pinMode(BRACO_ENABLE, OUTPUT);
  pinMode(BRACO_STEP, OUTPUT);
  pinMode(BRACO_DIR, OUTPUT);
  pinMode(VIB_ENABLE_PIN, OUTPUT);
  digitalWrite(CARRO_ENABLE, DESLIGADO); //Desliga o motor do carro
  digitalWrite(BRACO_ENABLE, DESLIGADO); //Desliga o motor do bracinho
  digitalWrite(VIB_ENABLE_PIN, LOW); //Desliga o SISTEMA DE VIBRAÇÃO
  converte();
  inputString.reserve(200);
}

void loop() {
  int bt = ler_pino();

  if (bt == BT_UP) {
    delay(DEBOUNCE);
    bt = ler_pino();
    if(bt == BT_UP){
      up_motion(RAPIDO);
      delay(DEBOUNCE*8);
    }
  }

  if (bt == BT_DOWN) {
    delay(DEBOUNCE);
    bt = ler_pino();
    if(bt == BT_DOWN){
      down_motion(RAPIDO,0);
      delay(DEBOUNCE*8);
    }
  }

  if (bt == BT_RIGHT) {
    delay(DEBOUNCE);
    bt = ler_pino();
    if(bt == BT_RIGHT){
      right_motion(RAPIDO);
      delay(DEBOUNCE*8);
    }
  }

  if (bt == BT_LEFT) {
    delay(DEBOUNCE);
    bt = ler_pino();
    if(bt == BT_LEFT){
      left_motion(RAPIDO);
      delay(DEBOUNCE*8);
    }
  }

  if (bt == BT_SET) {
    int a=0;
    while (bt == BT_SET && a<35) {
      delay(50);
      bt = ler_pino();
      a++;
    }

    if (a>30){
      delay(700);
    }
    else {
      rotina_principal();
    }
  }

  if (stringComplete) {
    if (inputString.length() == 6) {

      int milhar = (int)inputString.charAt(1) - 48;
      int centena = (int)inputString.charAt(2) - 48;
      int dezena = (int)inputString.charAt(3) - 48;
      int unidade = (int)inputString.charAt(4) - 48;
      valor_recebido = milhar * 1000 + centena * 100 + dezena * 10 + unidade;

      switch (inputString.charAt(0)) {
      case 'a':
        up_motion(RAPIDO);
        break;
      case 'b':
        down_motion(RAPIDO,0);
        break;
      case 'c':
        left_motion(RAPIDO);
        break;
      case 'd':
        right_motion(RAPIDO);
        break;
      case 'e':
        rotina_principal();
        break;
      case 'f':
        Serial.println("7");
        break;
      case 'g':
        tempo_imerso_p = valor_recebido * MINUTO;
        break;
      case 'h':
        tempo_imerso_n = valor_recebido * MINUTO;
        break;
      case 'i':
        tempo_lavagem = SEGUNDO * valor_recebido / 10;
        break;
      case 'j':
        tempo_secagem = MINUTO * valor_recebido / 10;
        break;
      case 'k':
        n_bicamadas = valor_recebido;
        break;
      case 'l':
        angulo = valor_recebido;
        break;
      case 'm':
        grau_por_passo_motor = valor_recebido / 10;
        break;
      case 'n':
        d1 = valor_recebido;
        break;
      case 'o':
        v_up = valor_recebido;
        break;
      case 'p':
        v_down = valor_recebido;
        break;
      case 'q':
        v_angular = valor_recebido;
        break;
      case 'r':
        digitalWrite(VIB_ENABLE_PIN, valor_recebido);
        break;
      default:
        break;
      }
      converte();
    }
    // clear the string:
    inputString = "";
    stringComplete = false;
  }
}

void up_motion(float multiplicador) {
  digitalWrite(BRACO_ENABLE, LIGADO);  //Liga o motor do braço
  digitalWrite(BRACO_DIR, SUBIR);

  for (int k = 0; k < voltas_vertical; k++) {
    for (int x = 0; x < passos_por_volta; x++) {
      passo(BRACO_STEP, t_up * multiplicador);
    }
  }

  digitalWrite(BRACO_ENABLE, DESLIGADO);  //Desliga o motor do braço
}

void down_motion(float multiplicador, int ligar_vib) {
  digitalWrite(BRACO_ENABLE, LIGADO); //Liga o motor do braço
  digitalWrite(BRACO_DIR, DESCER);//SOBE COM LOW
  digitalWrite(VIB_ENABLE_PIN, ligar_vib);

  for (int k = 0; k < voltas_vertical; k++) {
    for (int x = 0; x < passos_por_volta ; x++) {
      passo(BRACO_STEP, t_down * multiplicador);
    }
  }

  digitalWrite(BRACO_ENABLE, DESLIGADO);  //Desliga o motor do braço
  digitalWrite(VIB_ENABLE_PIN, LOW);
}

void right_motion(float multiplicador) {
  digitalWrite(CARRO_ENABLE, LIGADO);        //Liga o motor do carro
  digitalWrite(CARRO_DIR, DIREITA);

  for (float x = 0; x <= angulo; x = x + grau_por_passo) {
    passo(CARRO_STEP, t_angular * multiplicador);
  }

  digitalWrite(CARRO_ENABLE, DESLIGADO);    //Desliga o motor do carro
}

void left_motion(float multiplicador) {
  digitalWrite(CARRO_ENABLE, LIGADO);        //Liga o motor do carro
  digitalWrite(CARRO_DIR, ESQUERDA);

  for (float x = 0; x <= angulo; x = x + grau_por_passo) {
    passo(CARRO_STEP, t_angular * multiplicador);
  }

  digitalWrite(CARRO_ENABLE, DESLIGADO);      // DEsliga o motor do carro
}

void rotina_principal() {
  numero = 0;
  for (int y = 0; y < n_bicamadas; y++) {
    numero = y;
    down_motion(LENTO,1);
    delay(tempo_imerso_p);

    up_motion(LENTO);
    delay(tempo_espera);

    right_motion(LENTO);
    delay(tempo_espera);

    down_motion(LENTO,0);
    delay(tempo_lavagem);

    up_motion(LENTO);
    delay(tempo_secagem);

    right_motion(LENTO);
    delay(tempo_espera);

    down_motion(LENTO,1);
    delay(tempo_imerso_n);

    up_motion(LENTO);
    delay(tempo_espera);

    left_motion(LENTO);
    delay(tempo_espera);

    down_motion(LENTO,0);
    delay(tempo_lavagem);

    up_motion(LENTO);
    delay(tempo_secagem);

    left_motion(LENTO);
  }

}

void converte() {
  passos_por_volta = (int)360 / grau_por_passo;
  voltas_vertical = (int)(d1 / PITCH);
  grau_por_passo = grau_por_passo_motor / 8;
}

void passo(int pino, float tempo) {
  digitalWrite(pino, HIGH);
  delayMicroseconds(tempo);
  digitalWrite(pino, LOW);
  delayMicroseconds(tempo);
}


void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag
    // so the main loop can do something about it:
    if (inChar == '\n') {
      stringComplete = true;
    }
  }
}

int ler_pino(){
  int leitura = analogRead(A0);
  int apertado = 0;

  if (leitura >= (UP - ERRO) && leitura <= (UP + ERRO)) {
    apertado = 1;
  }
  if (leitura >= (DOWN - ERRO) && leitura <= (DOWN + ERRO)) {
    apertado = 2;
  }
  if (leitura >= (LEFT - ERRO) && leitura <= (LEFT + ERRO)) {
    apertado = 3;
  }
  if (leitura >= (RIGHT - ERRO) && leitura <= (RIGHT + ERRO)) {
    apertado = 4;
  }
  if (leitura >= (SET - ERRO) && leitura <= (SET + ERRO)) {
    apertado = 5;
  }
  return apertado;
}



