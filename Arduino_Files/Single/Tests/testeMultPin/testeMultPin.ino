#define ERRO 10
#define UP 168
#define DOWN 854
#define LEFT 339
#define RIGHT 681
#define SET 510
void setup() {

  Serial.begin(9600);
}

void loop() {
  int leitura = analogRead(A0);
  int apertado = 0;

  if (leitura >= (UP - ERRO) && leitura <= (UP + ERRO)) {
    apertado = 1;
  }
  if (leitura >= (DOWN - ERRO) && leitura <= (DOWN + ERRO)) {
    apertado = 2;
  }
  if (leitura >= (LEFT - ERRO) && leitura <= (LEFT + ERRO)) {
    apertado = 3;
  }
  if (leitura >= (RIGHT - ERRO) && leitura <= (RIGHT + ERRO)) {
    apertado = 4;
  }
  if (leitura >= (SET - ERRO) && leitura <= (SET + ERRO)) {
    apertado = 5;
  }

  Serial.println(apertado);
  delay(500);
}

