#include "TimerOne.h"
#define BCD1 2
#define BCD2 5
#define BCD3 4 
#define BCD4 3
#define DISP1 7
#define DISP2 6

int numero = 0;
int disp = 1;
String numero_str = "";


void setup() {
  pinMode(BCD1, OUTPUT);
  pinMode(BCD2, OUTPUT);
  pinMode(BCD3, OUTPUT);
  pinMode(BCD4, OUTPUT);
  pinMode(DISP1, OUTPUT);
  pinMode(DISP2, OUTPUT);
  Timer1.initialize(10000); // Inicializa o Timer1 e configura para um período de 0,5 segundos
  Timer1.attachInterrupt(callback); // Configura a função callback() como a função para ser chamada a cada interrupção do Timer1
}

void loop() {
  numero++;
  //printar(numero);
  delay(300);
  if (numero >= 99) {
    numero = -1;
  }
}

void callback() {
  String saida = "";
  switch (disp) {
    case 1:
      digitalWrite(DISP1, 1);
      digitalWrite(DISP2, 0);
      printar(int_to_disp(0));
      disp = 2;
      break;
    case 2:
      digitalWrite(DISP1, 0);
      digitalWrite(DISP2, 1);
      printar(int_to_disp(1));
      disp = 1;
      break;
  }
}

int int_to_disp(int index) {
  int work[] = {0, 0};
  if (numero < 10) {
    work[0] = 0;
    work[1] = numero;
  }
  if (numero >= 10 && numero < 20) {
    work[0] = 1;
    work[1] = numero - 10;
  }
  if (numero >= 20 && numero < 30) {
    work[0] = 2;
    work[1] = numero - 20;
  }
  if (numero >= 30 && numero < 40) {
    work[0] = 3;
    work[1] = numero - 30;
  }
  if (numero >= 40 && numero < 50) {
    work[0] = 4;
    work[1] = numero - 40;
  }
  if (numero >= 50 && numero < 60) {
    work[0] = 5;
    work[1] = numero - 50;
  }
  if (numero >= 60 && numero < 70) {
    work[0] = 6;
    work[1] = numero - 60;
  }
  if (numero >= 70 && numero < 80) {
    work[0] = 7;
    work[1] = numero - 70;
  }
  if (numero >= 80 && numero < 90) {
    work[0] = 8;
    work[1] = numero - 80;
  }
  if (numero >= 90 && numero < 100) {
    work[0] = 9;
    work[1] = numero - 90;
  }
  if (numero > 100 ) {
    work[0] = 9;
    work[1] = 9;
  }

  return work[index];
}


void printar(int entrada) {
  switch (entrada) {
    case 0:
      digitalWrite(BCD4, 0);
      digitalWrite(BCD3, 0);
      digitalWrite(BCD2, 0);
      digitalWrite(BCD1, 0);
      break;
    case 1:
      digitalWrite(BCD4, 0);
      digitalWrite(BCD3, 0);
      digitalWrite(BCD2, 0);
      digitalWrite(BCD1, 1);
      break;
    case 2:
      digitalWrite(BCD4, 0);
      digitalWrite(BCD3, 0);
      digitalWrite(BCD2, 1);
      digitalWrite(BCD1, 0);
      break;
    case 3:
      digitalWrite(BCD4, 0);
      digitalWrite(BCD3, 0);
      digitalWrite(BCD2, 1);
      digitalWrite(BCD1, 1);
      break;
    case 4:
      digitalWrite(BCD4, 0);
      digitalWrite(BCD3, 1);
      digitalWrite(BCD2, 0);
      digitalWrite(BCD1, 0);
      break;
    case 5:
      digitalWrite(BCD4, 0);
      digitalWrite(BCD3, 1);
      digitalWrite(BCD2, 0);
      digitalWrite(BCD1, 1);
      break;
    case 6:
      digitalWrite(BCD4, 0);
      digitalWrite(BCD3, 1);
      digitalWrite(BCD2, 1);
      digitalWrite(BCD1, 0);
      break;
    case 7:
      digitalWrite(BCD4, 0);
      digitalWrite(BCD3, 1);
      digitalWrite(BCD2, 1);
      digitalWrite(BCD1, 1);
      break;
    case 8:
      digitalWrite(BCD4, 1);
      digitalWrite(BCD3, 0);
      digitalWrite(BCD2, 0);
      digitalWrite(BCD1, 0);
      break;
    case 9:
      digitalWrite(BCD4, 1);
      digitalWrite(BCD3, 0);
      digitalWrite(BCD2, 0);
      digitalWrite(BCD1, 1);
      break;
  }
}
