#include "TimerOne.h"
#define BCD1 2
#define BCD2 5
#define BCD3 4 
#define BCD4 3
#define DISP1 7
#define DISP2 6
#define ERRO 10
#define UP 167
#define DOWN 854
#define LEFT 335
#define RIGHT 673
#define SET 504
#define BT_SET 5
#define BT_UP 1
#define BT_DOWN 2

int numero = 0;
int disp = 1;
int contagem = 0;
boolean setando = false;
boolean mostrar = true;

int n_bicamadas = 30;

void setup() {
    Serial.begin(9600);
  pinMode(BCD1, OUTPUT);
  pinMode(BCD2, OUTPUT);
  pinMode(BCD3, OUTPUT);
  pinMode(BCD4, OUTPUT);
  pinMode(DISP1, OUTPUT);
  pinMode(DISP2, OUTPUT);
  Timer1.initialize(10000); // Inicializa o Timer1 e configura para um período de 0,5 segundos
  Timer1.attachInterrupt(callback); // Configura a função callback() como a função para ser chamada a cada interrupção do Timer1
}

void loop() {
  int bt = ler_pino();
  if (bt == BT_SET) {
    int a=0;
    while (bt == BT_SET && a<35) {
      delay(50);
      bt = ler_pino();
      a++;
    }

    if (a>30){
      mudar_bicamadas();
    }
    else {
      bt=BT_SET;
    }
  }
  numero = bt;
}

void callback() {
  if (mostrar){
    switch (disp) {
    case 1:
      digitalWrite(DISP1, 1);
      digitalWrite(DISP2, 0);
      printar(int_to_disp(0));
      disp = 2;
      break;
    case 2:
      digitalWrite(DISP1, 0);
      digitalWrite(DISP2, 1);
      printar(int_to_disp(1));
      disp = 1;
      break;
    }
  }
  else{
    digitalWrite(DISP1, 0);
    digitalWrite(DISP2, 0);
  }
  contagem++;
  if (contagem > 30){
    contagem = 0;
    if (setando){
      mostrar = !mostrar;
    }
    else {
      mostrar = true;
    }
  }
}

int int_to_disp(int index) {
  int work[] = {
    0, 0                  };
  if (numero < 10) {
    work[0] = 0;
    work[1] = numero;
  }
  if (numero >= 10 && numero < 20) {
    work[0] = 1;
    work[1] = numero - 10;
  }
  if (numero >= 20 && numero < 30) {
    work[0] = 2;
    work[1] = numero - 20;
  }
  if (numero >= 30 && numero < 40) {
    work[0] = 3;
    work[1] = numero - 30;
  }
  if (numero >= 40 && numero < 50) {
    work[0] = 4;
    work[1] = numero - 40;
  }
  if (numero >= 50 && numero < 60) {
    work[0] = 5;
    work[1] = numero - 50;
  }
  if (numero >= 60 && numero < 70) {
    work[0] = 6;
    work[1] = numero - 60;
  }
  if (numero >= 70 && numero < 80) {
    work[0] = 7;
    work[1] = numero - 70;
  }
  if (numero >= 80 && numero < 90) {
    work[0] = 8;
    work[1] = numero - 80;
  }
  if (numero >= 90 && numero < 100) {
    work[0] = 9;
    work[1] = numero - 90;
  }
  if (numero > 100 ) {
    work[0] = 9;
    work[1] = 9;
  }

  return work[index];
}


void printar(int entrada) {
  switch (entrada) {
  case 0:
    digitalWrite(BCD4, 0);
    digitalWrite(BCD3, 0);
    digitalWrite(BCD2, 0);
    digitalWrite(BCD1, 0);
    break;
  case 1:
    digitalWrite(BCD4, 0);
    digitalWrite(BCD3, 0);
    digitalWrite(BCD2, 0);
    digitalWrite(BCD1, 1);
    break;
  case 2:
    digitalWrite(BCD4, 0);
    digitalWrite(BCD3, 0);
    digitalWrite(BCD2, 1);
    digitalWrite(BCD1, 0);
    break;
  case 3:
    digitalWrite(BCD4, 0);
    digitalWrite(BCD3, 0);
    digitalWrite(BCD2, 1);
    digitalWrite(BCD1, 1);
    break;
  case 4:
    digitalWrite(BCD4, 0);
    digitalWrite(BCD3, 1);
    digitalWrite(BCD2, 0);
    digitalWrite(BCD1, 0);
    break;
  case 5:
    digitalWrite(BCD4, 0);
    digitalWrite(BCD3, 1);
    digitalWrite(BCD2, 0);
    digitalWrite(BCD1, 1);
    break;
  case 6:
    digitalWrite(BCD4, 0);
    digitalWrite(BCD3, 1);
    digitalWrite(BCD2, 1);
    digitalWrite(BCD1, 0);
    break;
  case 7:
    digitalWrite(BCD4, 0);
    digitalWrite(BCD3, 1);
    digitalWrite(BCD2, 1);
    digitalWrite(BCD1, 1);
    break;
  case 8:
    digitalWrite(BCD4, 1);
    digitalWrite(BCD3, 0);
    digitalWrite(BCD2, 0);
    digitalWrite(BCD1, 0);
    break;
  case 9:
    digitalWrite(BCD4, 1);
    digitalWrite(BCD3, 0);
    digitalWrite(BCD2, 0);
    digitalWrite(BCD1, 1);
    break;
  }
}

int ler_pino(){
  int leitura = analogRead(A0);
  int apertado = 0;

  if (leitura >= (UP - ERRO) && leitura <= (UP + ERRO)) {
    apertado = 1;
  }
  if (leitura >= (DOWN - ERRO) && leitura <= (DOWN + ERRO)) {
    apertado = 2;
  }
  if (leitura >= (LEFT - ERRO) && leitura <= (LEFT + ERRO)) {
    apertado = 3;
  }
  if (leitura >= (RIGHT - ERRO) && leitura <= (RIGHT + ERRO)) {
    apertado = 4;
  }
  if (leitura >= (SET - ERRO) && leitura <= (SET + ERRO)) {
    apertado = 5;
  }
  return apertado;
}

void mudar_bicamadas(){
  setando = true;

  while(setando){
    numero = n_bicamadas;
    int bt = ler_pino();
    if(bt == BT_UP){
      n_bicamadas++;
    }
    if(bt == BT_DOWN){
      n_bicamadas--;
    }
    if (bt == BT_SET) {
      int a=0;
      while (bt == BT_SET && a<35) {
        delay(50);
        bt = ler_pino();
        a++;
      }

      if (a>30){
        setando = false;
      }
    }
    delay(200);
    //setando = false;
  }
}






