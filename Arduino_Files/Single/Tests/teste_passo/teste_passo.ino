#define SEGUNDO  1000   //conversão segundo para msegundo
#define MINUTO 60000    //conversão minuto para msegundo
#define PITCH 0.8
#define CARRO_ENABLE 11 //enable motor de cima
#define CARRO_DIR 13    //dir motor de cima
#define CARRO_STEP 12   //step motor de cima
#define BRACO_STEP 9    //step motor de baixo
#define BRACO_ENABLE 10 //enable motor de baixo
#define BRACO_DIR 8     //dir motor de baixo
#define LIGADO LOW
#define DESLIGADO HIGH
#define SUBIR LOW
#define DESCER HIGH
#define ESQUERDA LOW
#define DIREITA HIGH

int n_bicamadas = 30; // número de bicamadas

int angulo = 90;               //ângulo de deslocamento lateral

long tempo_imerso_p = 10 * MINUTO;   //tempo de imersao do postivo
long tempo_imerso_n = 10 * MINUTO;   //tempo de imersao do negativo
long tempo_secagem = 1 * MINUTO;     //tempo de secagem
long tempo_lavagem = 1 * SEGUNDO;    //tempo de lavagem da rotina
long tempo_espera = 1 * SEGUNDO;     //tempo de espera

//CUIDADO! O # de voltas deve ser sempre inteiro.
float d1 = 10;                       // deslocamento vertical em mm
int voltas_vertical = (int)(d1 / PITCH); // # voltas para deslocamento d1
float grau_por_passo_motor = 1.8;
float grau_por_passo = grau_por_passo_motor / 8;
int passos_por_volta = (int)360 / grau_por_passo;

int v_up = 10;        //velocidade de subida em mm/min
int v_down = 60;      //velocidade de descida em mm/min
int v_angular = 5;    //velocidade de angular
double t_up = (24E6) / (v_up*passos_por_volta);         // tempo de subida
double t_down = (24E6) / (v_down*passos_por_volta);     // tempo de descida
double t_angular = (24E6) / (v_angular*passos_por_volta); // velocidade para girar

void setup() {
  pinMode(CARRO_ENABLE, OUTPUT);
  pinMode(CARRO_STEP, OUTPUT);
  pinMode(CARRO_DIR, OUTPUT);
  pinMode(BRACO_ENABLE, OUTPUT);
  pinMode(BRACO_STEP, OUTPUT);
  pinMode(BRACO_DIR, OUTPUT);
  digitalWrite(CARRO_ENABLE, DESLIGADO); //Desliga o motor do carro
  digitalWrite(BRACO_ENABLE, DESLIGADO); //Desliga o motor do bracinho
  converte();
}

void loop() {
  digitalWrite(BRACO_ENABLE, LIGADO); //Liga o motor do braço
  digitalWrite(BRACO_DIR, DESCER);//SOBE COM LOW
  for (int k = 0; k < voltas_vertical; k++) {
    for (int x = 0; x < passos_por_volta ; x++) {
      passo(BRACO_STEP, t_down * 0.3);
    }
  }
  digitalWrite(BRACO_ENABLE, DESLIGADO);  //Desliga o motor do braço

  digitalWrite(BRACO_ENABLE, LIGADO); //Liga o motor do braço
  digitalWrite(BRACO_DIR, SUBIR);//SOBE COM LOW
  for (int k = 0; k < voltas_vertical; k++) {
    for (int x = 0; x < passos_por_volta ; x++) {
      passo(BRACO_STEP, t_down * 0.3);
    }
  }
  digitalWrite(BRACO_ENABLE, DESLIGADO);  //Desliga o motor do braço


  digitalWrite(CARRO_ENABLE, LIGADO);        //Liga o motor do carro
  digitalWrite(CARRO_DIR, ESQUERDA);
  for (float x = 0; x <= angulo; x = x + grau_por_passo) {
    passo(CARRO_STEP, t_angular * 0.3);
  }
  digitalWrite(CARRO_ENABLE, DESLIGADO);      // DEsliga o motor do carro

  
  digitalWrite(CARRO_ENABLE, LIGADO);        //Liga o motor do carro
  digitalWrite(CARRO_DIR, DIREITA);
  for (float x = 0; x <= angulo; x = x + grau_por_passo) {
    passo(CARRO_STEP, t_angular * 0.3);
  }
  digitalWrite(CARRO_ENABLE, DESLIGADO);      // DEsliga o motor do carro


}


void converte() {
  passos_por_volta = (int)360 / grau_por_passo;
  voltas_vertical = (int)(d1 / PITCH);
  grau_por_passo = grau_por_passo_motor / 8;
}

void passo(int pino, float tempo) {
  digitalWrite(pino, HIGH);
  delayMicroseconds(tempo);
  digitalWrite(pino, LOW);
  delayMicroseconds(tempo);
}

