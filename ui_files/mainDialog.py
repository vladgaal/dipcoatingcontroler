# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mainDialog.ui'
#
# Created: Fri Nov 10 13:29:35 2017
#      by: pyside-uic 0.2.15 running on PySide 1.2.4
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(680, 320)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(Dialog.sizePolicy().hasHeightForWidth())
        Dialog.setSizePolicy(sizePolicy)
        Dialog.setMinimumSize(QtCore.QSize(680, 320))
        Dialog.setMaximumSize(QtCore.QSize(680, 320))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/setas/icons/Layers.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        Dialog.setWindowIcon(icon)
        Dialog.setStyleSheet("\n"
"QDialog#Dialog{\n"
"    background-color:qlineargradient(spread:pad, x1:0, y1:1, x2:1, y2:0, stop:0.357955 rgba(50, 50, 50, 255), stop:1 rgba(116, 116, 116, 255));\n"
"min-width: 680px;\n"
"max-width: 680px;\n"
"min-height: 320px;\n"
"max-height: 320px;\n"
"}\n"
"\n"
"QGroupBox{\n"
"border: 2px solid rgb(190, 190, 190);\n"
"border-radius: 10px;\n"
"margin-top: 2ex\n"
"}\n"
"\n"
"QGroupBox::title{\n"
"subcontrol-origin: margin;\n"
"subcontrol-position: top left;\n"
"padding: 0 3px;\n"
"color: rgb(255, 255, 255);\n"
"}\n"
"\n"
"QLabel#label, QLabel#label_2, QLabel#label_4, QLabel#label_3, QLabel#label_5, QLabel#label_6, QLabel#label_7{\n"
"color:white;\n"
"}\n"
"\n"
"QLineEdit{\n"
"border: 2px solid rgb(113, 113, 113);\n"
"border-radius: 5px;\n"
"padding: 0px;\n"
"}\n"
"\n"
"QDial{\n"
"background-color: rgb(175, 175, 175);\n"
"}\n"
"\n"
"QPushButton#conectarBt, QPushButton#desconectarBt, QPushButton#enviarBt, QPushButton#refreshBt{\n"
"background-color:qradialgradient(spread:pad, cx:0.505955, cy:0.43, radius:1.578, fx:0.506, fy:0.024, stop:0 rgba(255, 255, 255, 255), stop:1 rgba(0, 0, 0, 255));\n"
"border: 1px solid black;\n"
"border-radius: 5px;\n"
"}\n"
"\n"
"QPushButton#conectarBt:pressed, QPushButton#desconectarBt:pressed, QPushButton#enviarBt:pressed, QPushButton#refreshBt:pressed{\n"
"background-color:qradialgradient(spread:pad, cx:0.505955, cy:0.43, radius:1.578, fx:0.517, fy:0.979, stop:0 rgba(255, 255, 255, 255), stop:1 rgba(0, 0, 0, 255));\n"
"border: 1px solid white;\n"
"}\n"
"\n"
"QPushButton#conectarBt:hover, QPushButton#desconectarBt:hover, QPushButton#enviarBt:hover, QPushButton#refreshBt:hover{\n"
"border: 1px solid white;\n"
"}\n"
"\n"
"QToolButton#upBt{\n"
"background-color: qradialgradient(spread:pad, cx:0.5, cy:0.5, radius:2, fx:0.5, fy:1, stop:0 rgba(200, 200, 200, 255), stop:1 rgba(117, 117, 117, 255));\n"
"border: 1px solid black;\n"
"border-radius: 5px;\n"
"}\n"
"\n"
"QToolButton#downBt{\n"
"background-color: qradialgradient(spread:pad, cx:0.5, cy:0.5, radius:2, fx:0.5, fy:0, stop:0 rgba(200, 200, 200, 255), stop:1 rgba(117, 117, 117, 255));\n"
"border: 1px solid black;\n"
"border-radius: 5px;\n"
"}\n"
"\n"
"QToolButton#leftBt{\n"
"background-color: qradialgradient(spread:pad, cx:0.5, cy:0.5, radius:2, fx:1, fy:0.5, stop:0 rgba(200, 200, 200, 255), stop:1 rgba(117, 117, 117, 255));\n"
"border: 1px solid black;\n"
"border-radius: 5px;\n"
"}\n"
"\n"
"QToolButton#rightBt{\n"
"background-color:qradialgradient(spread:pad, cx:0.5, cy:0.5, radius:2, fx:0, fy:0.5, stop:0 rgba(200, 200, 200, 255), stop:1 rgba(117, 117, 117, 255));\n"
"border: 1px solid black;\n"
"border-radius: 5px;\n"
"}\n"
"\n"
"QToolButton#setBt{\n"
"background-color:qradialgradient(spread:pad, cx:0.5, cy:0.5, radius:2, fx:0.5, fy:0.5, stop:0 rgba(212, 212, 212, 255), stop:0.857955 rgba(172, 172, 172, 255));\n"
"border: 1px solid black;\n"
"border-radius: 5px;\n"
"}\n"
"\n"
"QToolButton#upBt:hover, QToolButton#downBt:hover, QToolButton#leftBt:hover, QToolButton#rightBt:hover, QToolButton#setBt:hover{\n"
"border: 1px solid white;\n"
"}\n"
"\n"
"")
        self.movimentacaoBox = QtGui.QGroupBox(Dialog)
        self.movimentacaoBox.setGeometry(QtCore.QRect(10, 10, 141, 151))
        self.movimentacaoBox.setObjectName("movimentacaoBox")
        self.upBt = QtGui.QToolButton(self.movimentacaoBox)
        self.upBt.setGeometry(QtCore.QRect(50, 20, 40, 40))
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(":/setas/icons/UArrow.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.upBt.setIcon(icon1)
        self.upBt.setIconSize(QtCore.QSize(32, 32))
        self.upBt.setObjectName("upBt")
        self.leftBt = QtGui.QToolButton(self.movimentacaoBox)
        self.leftBt.setGeometry(QtCore.QRect(10, 60, 40, 40))
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap(":/setas/icons/LArrow.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.leftBt.setIcon(icon2)
        self.leftBt.setIconSize(QtCore.QSize(32, 32))
        self.leftBt.setObjectName("leftBt")
        self.rightBt = QtGui.QToolButton(self.movimentacaoBox)
        self.rightBt.setGeometry(QtCore.QRect(90, 60, 40, 40))
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap(":/setas/icons/RArrow.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.rightBt.setIcon(icon3)
        self.rightBt.setIconSize(QtCore.QSize(32, 32))
        self.rightBt.setArrowType(QtCore.Qt.NoArrow)
        self.rightBt.setObjectName("rightBt")
        self.downBt = QtGui.QToolButton(self.movimentacaoBox)
        self.downBt.setGeometry(QtCore.QRect(50, 100, 40, 40))
        icon4 = QtGui.QIcon()
        icon4.addPixmap(QtGui.QPixmap(":/setas/icons/DArrow.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.downBt.setIcon(icon4)
        self.downBt.setIconSize(QtCore.QSize(32, 32))
        self.downBt.setObjectName("downBt")
        self.setBt = QtGui.QToolButton(self.movimentacaoBox)
        self.setBt.setGeometry(QtCore.QRect(50, 60, 40, 40))
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setWeight(75)
        font.setBold(True)
        self.setBt.setFont(font)
        self.setBt.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.setBt.setObjectName("setBt")
        self.comunicacaoBox = QtGui.QGroupBox(Dialog)
        self.comunicacaoBox.setGeometry(QtCore.QRect(10, 170, 401, 131))
        self.comunicacaoBox.setObjectName("comunicacaoBox")
        self.label = QtGui.QLabel(self.comunicacaoBox)
        self.label.setGeometry(QtCore.QRect(10, 20, 70, 20))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.label_2 = QtGui.QLabel(self.comunicacaoBox)
        self.label_2.setGeometry(QtCore.QRect(10, 50, 70, 20))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_2.setFont(font)
        self.label_2.setObjectName("label_2")
        self.portaBox = QtGui.QComboBox(self.comunicacaoBox)
        self.portaBox.setGeometry(QtCore.QRect(90, 20, 69, 22))
        self.portaBox.setObjectName("portaBox")
        self.portaBox.addItem("")
        self.portaBox.addItem("")
        self.baudBox = QtGui.QComboBox(self.comunicacaoBox)
        self.baudBox.setGeometry(QtCore.QRect(90, 50, 69, 22))
        self.baudBox.setObjectName("baudBox")
        self.baudBox.addItem("")
        self.baudBox.setItemText(0, "")
        self.baudBox.addItem("")
        self.conectarBt = QtGui.QPushButton(self.comunicacaoBox)
        self.conectarBt.setGeometry(QtCore.QRect(180, 20, 90, 61))
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setWeight(50)
        font.setBold(False)
        self.conectarBt.setFont(font)
        self.conectarBt.setFocusPolicy(QtCore.Qt.WheelFocus)
        self.conectarBt.setObjectName("conectarBt")
        self.desconectarBt = QtGui.QPushButton(self.comunicacaoBox)
        self.desconectarBt.setGeometry(QtCore.QRect(279, 20, 111, 30))
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setWeight(50)
        font.setBold(False)
        self.desconectarBt.setFont(font)
        self.desconectarBt.setFocusPolicy(QtCore.Qt.WheelFocus)
        self.desconectarBt.setObjectName("desconectarBt")
        self.line = QtGui.QFrame(self.comunicacaoBox)
        self.line.setGeometry(QtCore.QRect(170, 10, 3, 75))
        self.line.setFrameShape(QtGui.QFrame.VLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName("line")
        self.conexaoTexto = QtGui.QLabel(self.comunicacaoBox)
        self.conexaoTexto.setGeometry(QtCore.QRect(20, 90, 361, 41))
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setWeight(75)
        font.setBold(True)
        self.conexaoTexto.setFont(font)
        self.conexaoTexto.setAlignment(QtCore.Qt.AlignCenter)
        self.conexaoTexto.setObjectName("conexaoTexto")
        self.line_3 = QtGui.QFrame(self.comunicacaoBox)
        self.line_3.setGeometry(QtCore.QRect(10, 80, 380, 20))
        self.line_3.setFrameShape(QtGui.QFrame.HLine)
        self.line_3.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_3.setObjectName("line_3")
        self.refreshBt = QtGui.QPushButton(self.comunicacaoBox)
        self.refreshBt.setGeometry(QtCore.QRect(279, 50, 111, 30))
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setWeight(50)
        font.setBold(False)
        self.refreshBt.setFont(font)
        self.refreshBt.setFocusPolicy(QtCore.Qt.WheelFocus)
        self.refreshBt.setObjectName("refreshBt")
        self.outrosBox = QtGui.QGroupBox(Dialog)
        self.outrosBox.setGeometry(QtCore.QRect(550, 170, 121, 131))
        self.outrosBox.setObjectName("outrosBox")
        self.label_4 = QtGui.QLabel(self.outrosBox)
        self.label_4.setGeometry(QtCore.QRect(10, 20, 101, 20))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_4.setFont(font)
        self.label_4.setObjectName("label_4")
        self.enviarBt = QtGui.QPushButton(self.outrosBox)
        self.enviarBt.setGeometry(QtCore.QRect(10, 70, 101, 51))
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setWeight(75)
        font.setBold(True)
        self.enviarBt.setFont(font)
        self.enviarBt.setObjectName("enviarBt")
        self.comandoEspecial = QtGui.QLineEdit(self.outrosBox)
        self.comandoEspecial.setGeometry(QtCore.QRect(10, 40, 101, 21))
        self.comandoEspecial.setObjectName("comandoEspecial")
        self.imersaoPosGBox = QtGui.QGroupBox(Dialog)
        self.imersaoPosGBox.setGeometry(QtCore.QRect(160, 10, 121, 151))
        self.imersaoPosGBox.setObjectName("imersaoPosGBox")
        self.imersaoPosDial = QtGui.QDial(self.imersaoPosGBox)
        self.imersaoPosDial.setGeometry(QtCore.QRect(10, 20, 101, 91))
        self.imersaoPosDial.setMinimum(1)
        self.imersaoPosDial.setMaximum(999)
        self.imersaoPosDial.setProperty("value", 5)
        self.imersaoPosDial.setNotchesVisible(True)
        self.imersaoPosDial.setObjectName("imersaoPosDial")
        self.imersaoPosBox = QtGui.QSpinBox(self.imersaoPosGBox)
        self.imersaoPosBox.setGeometry(QtCore.QRect(10, 120, 101, 22))
        self.imersaoPosBox.setMinimum(1)
        self.imersaoPosBox.setMaximum(999)
        self.imersaoPosBox.setProperty("value", 5)
        self.imersaoPosBox.setObjectName("imersaoPosBox")
        self.imersaoNegGBox = QtGui.QGroupBox(Dialog)
        self.imersaoNegGBox.setGeometry(QtCore.QRect(290, 10, 121, 151))
        self.imersaoNegGBox.setObjectName("imersaoNegGBox")
        self.imersaoNegDial = QtGui.QDial(self.imersaoNegGBox)
        self.imersaoNegDial.setGeometry(QtCore.QRect(10, 20, 101, 91))
        self.imersaoNegDial.setMinimum(1)
        self.imersaoNegDial.setMaximum(999)
        self.imersaoNegDial.setProperty("value", 5)
        self.imersaoNegDial.setNotchesVisible(True)
        self.imersaoNegDial.setObjectName("imersaoNegDial")
        self.imersaoNegBox = QtGui.QSpinBox(self.imersaoNegGBox)
        self.imersaoNegBox.setGeometry(QtCore.QRect(10, 120, 101, 22))
        self.imersaoNegBox.setMinimum(1)
        self.imersaoNegBox.setMaximum(999)
        self.imersaoNegBox.setProperty("value", 5)
        self.imersaoNegBox.setObjectName("imersaoNegBox")
        self.lavagemGBox = QtGui.QGroupBox(Dialog)
        self.lavagemGBox.setGeometry(QtCore.QRect(420, 10, 121, 151))
        self.lavagemGBox.setObjectName("lavagemGBox")
        self.lavagemDial = QtGui.QDial(self.lavagemGBox)
        self.lavagemDial.setGeometry(QtCore.QRect(10, 20, 101, 91))
        self.lavagemDial.setMinimum(5)
        self.lavagemDial.setMaximum(20)
        self.lavagemDial.setNotchesVisible(True)
        self.lavagemDial.setObjectName("lavagemDial")
        self.lavagemBox = QtGui.QDoubleSpinBox(self.lavagemGBox)
        self.lavagemBox.setGeometry(QtCore.QRect(10, 120, 101, 22))
        self.lavagemBox.setPrefix("")
        self.lavagemBox.setDecimals(1)
        self.lavagemBox.setMinimum(0.5)
        self.lavagemBox.setMaximum(2.0)
        self.lavagemBox.setSingleStep(0.1)
        self.lavagemBox.setProperty("value", 1.0)
        self.lavagemBox.setObjectName("lavagemBox")
        self.secagemGBox = QtGui.QGroupBox(Dialog)
        self.secagemGBox.setGeometry(QtCore.QRect(550, 10, 121, 151))
        self.secagemGBox.setObjectName("secagemGBox")
        self.secagemDial = QtGui.QDial(self.secagemGBox)
        self.secagemDial.setGeometry(QtCore.QRect(10, 20, 101, 91))
        self.secagemDial.setMinimum(10)
        self.secagemDial.setMaximum(50)
        self.secagemDial.setPageStep(1)
        self.secagemDial.setProperty("value", 20)
        self.secagemDial.setTracking(True)
        self.secagemDial.setInvertedAppearance(False)
        self.secagemDial.setInvertedControls(False)
        self.secagemDial.setWrapping(False)
        self.secagemDial.setNotchesVisible(True)
        self.secagemDial.setObjectName("secagemDial")
        self.secagemBox = QtGui.QDoubleSpinBox(self.secagemGBox)
        self.secagemBox.setGeometry(QtCore.QRect(10, 120, 101, 22))
        self.secagemBox.setPrefix("")
        self.secagemBox.setDecimals(1)
        self.secagemBox.setMinimum(1.0)
        self.secagemBox.setMaximum(5.0)
        self.secagemBox.setSingleStep(0.1)
        self.secagemBox.setProperty("value", 2.0)
        self.secagemBox.setObjectName("secagemBox")
        self.outrosBox_2 = QtGui.QGroupBox(Dialog)
        self.outrosBox_2.setGeometry(QtCore.QRect(420, 170, 121, 131))
        self.outrosBox_2.setObjectName("outrosBox_2")
        self.bicamadasBox = QtGui.QSpinBox(self.outrosBox_2)
        self.bicamadasBox.setGeometry(QtCore.QRect(61, 20, 50, 22))
        self.bicamadasBox.setMinimum(1)
        self.bicamadasBox.setMaximum(999)
        self.bicamadasBox.setProperty("value", 30)
        self.bicamadasBox.setObjectName("bicamadasBox")
        self.anguloBox = QtGui.QSpinBox(self.outrosBox_2)
        self.anguloBox.setGeometry(QtCore.QRect(61, 50, 50, 22))
        self.anguloBox.setMaximum(180)
        self.anguloBox.setSingleStep(10)
        self.anguloBox.setProperty("value", 30)
        self.anguloBox.setObjectName("anguloBox")
        self.label_3 = QtGui.QLabel(self.outrosBox_2)
        self.label_3.setGeometry(QtCore.QRect(10, 20, 50, 22))
        self.label_3.setObjectName("label_3")
        self.label_5 = QtGui.QLabel(self.outrosBox_2)
        self.label_5.setGeometry(QtCore.QRect(10, 50, 50, 22))
        self.label_5.setObjectName("label_5")
        self.label_6 = QtGui.QLabel(self.outrosBox_2)
        self.label_6.setGeometry(QtCore.QRect(10, 70, 50, 40))
        self.label_6.setObjectName("label_6")
        self.verticalBox = QtGui.QSpinBox(self.outrosBox_2)
        self.verticalBox.setGeometry(QtCore.QRect(60, 80, 50, 22))
        self.verticalBox.setMinimum(1)
        self.verticalBox.setMaximum(100)
        self.verticalBox.setSingleStep(10)
        self.verticalBox.setProperty("value", 40)
        self.verticalBox.setObjectName("verticalBox")
        self.label_7 = QtGui.QLabel(Dialog)
        self.label_7.setGeometry(QtCore.QRect(10, 300, 631, 20))
        self.label_7.setObjectName("label_7")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QtGui.QApplication.translate("Dialog", "Dialog", None, QtGui.QApplication.UnicodeUTF8))
        self.movimentacaoBox.setTitle(QtGui.QApplication.translate("Dialog", "Movement", None, QtGui.QApplication.UnicodeUTF8))
        self.upBt.setText(QtGui.QApplication.translate("Dialog", "...", None, QtGui.QApplication.UnicodeUTF8))
        self.leftBt.setText(QtGui.QApplication.translate("Dialog", "...", None, QtGui.QApplication.UnicodeUTF8))
        self.rightBt.setText(QtGui.QApplication.translate("Dialog", "...", None, QtGui.QApplication.UnicodeUTF8))
        self.downBt.setText(QtGui.QApplication.translate("Dialog", "...", None, QtGui.QApplication.UnicodeUTF8))
        self.setBt.setText(QtGui.QApplication.translate("Dialog", "Go!", None, QtGui.QApplication.UnicodeUTF8))
        self.comunicacaoBox.setTitle(QtGui.QApplication.translate("Dialog", "Connection", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("Dialog", "Port:", None, QtGui.QApplication.UnicodeUTF8))
        self.label_2.setText(QtGui.QApplication.translate("Dialog", "Baud Rate:", None, QtGui.QApplication.UnicodeUTF8))
        self.portaBox.setItemText(0, QtGui.QApplication.translate("Dialog", "COM1", None, QtGui.QApplication.UnicodeUTF8))
        self.portaBox.setItemText(1, QtGui.QApplication.translate("Dialog", "COM2", None, QtGui.QApplication.UnicodeUTF8))
        self.baudBox.setItemText(1, QtGui.QApplication.translate("Dialog", "9600", None, QtGui.QApplication.UnicodeUTF8))
        self.conectarBt.setText(QtGui.QApplication.translate("Dialog", "Connect", None, QtGui.QApplication.UnicodeUTF8))
        self.desconectarBt.setText(QtGui.QApplication.translate("Dialog", "Disconnect", None, QtGui.QApplication.UnicodeUTF8))
        self.conexaoTexto.setText(QtGui.QApplication.translate("Dialog", "Arduino Desconectado", None, QtGui.QApplication.UnicodeUTF8))
        self.refreshBt.setText(QtGui.QApplication.translate("Dialog", "Refresh ports", None, QtGui.QApplication.UnicodeUTF8))
        self.outrosBox.setTitle(QtGui.QApplication.translate("Dialog", "Others", None, QtGui.QApplication.UnicodeUTF8))
        self.label_4.setText(QtGui.QApplication.translate("Dialog", "Command:", None, QtGui.QApplication.UnicodeUTF8))
        self.enviarBt.setText(QtGui.QApplication.translate("Dialog", "Send", None, QtGui.QApplication.UnicodeUTF8))
        self.imersaoPosGBox.setTitle(QtGui.QApplication.translate("Dialog", "Positive immersion", None, QtGui.QApplication.UnicodeUTF8))
        self.imersaoPosBox.setSuffix(QtGui.QApplication.translate("Dialog", " min", None, QtGui.QApplication.UnicodeUTF8))
        self.imersaoNegGBox.setTitle(QtGui.QApplication.translate("Dialog", "Negative immersion", None, QtGui.QApplication.UnicodeUTF8))
        self.imersaoNegBox.setSuffix(QtGui.QApplication.translate("Dialog", " min", None, QtGui.QApplication.UnicodeUTF8))
        self.lavagemGBox.setTitle(QtGui.QApplication.translate("Dialog", "Washing time", None, QtGui.QApplication.UnicodeUTF8))
        self.lavagemBox.setSuffix(QtGui.QApplication.translate("Dialog", " sec", None, QtGui.QApplication.UnicodeUTF8))
        self.secagemGBox.setTitle(QtGui.QApplication.translate("Dialog", "Drying time ", None, QtGui.QApplication.UnicodeUTF8))
        self.secagemBox.setSuffix(QtGui.QApplication.translate("Dialog", " min", None, QtGui.QApplication.UnicodeUTF8))
        self.outrosBox_2.setTitle(QtGui.QApplication.translate("Dialog", "Setups", None, QtGui.QApplication.UnicodeUTF8))
        self.anguloBox.setSuffix(QtGui.QApplication.translate("Dialog", "°", None, QtGui.QApplication.UnicodeUTF8))
        self.label_3.setText(QtGui.QApplication.translate("Dialog", "Bilayers:", None, QtGui.QApplication.UnicodeUTF8))
        self.label_5.setText(QtGui.QApplication.translate("Dialog", "Angle:", None, QtGui.QApplication.UnicodeUTF8))
        self.label_6.setText(QtGui.QApplication.translate("Dialog", "Vertical\n"
"move:", None, QtGui.QApplication.UnicodeUTF8))
        self.verticalBox.setSuffix(QtGui.QApplication.translate("Dialog", "mm", None, QtGui.QApplication.UnicodeUTF8))
        self.label_7.setText(QtGui.QApplication.translate("Dialog", "© 2022 Copyright: Crafted with Love by "
                                                                    "Vladimir Gaal in association with GFNMN-Unicamp. "
                                                                    "- Feb/2022", None, QtGui.QApplication.UnicodeUTF8))

import icones_rc
